import { IReferralGroup } from "./IReferralGroup";
import { IReferralType } from "./IReferralType";
import { IStudent } from "./IStudent";
import { ICampus } from "./ICampus";
import { IReferralReason } from "./IReferralReason";

export interface IDashboard {
    ReferralId: number,
    StudentId: number,
    CampusId: number,
    Grade: string,
    TeacherId: number,
    DriverId: number,
    RouteId: number,
    ReferralTypeId: number,
    ReferralGroupId: number,
    Date?: Date,
    ReferralReasonsId: number,
    CreatedBy: string,
    DtCreated?: Date,
    LastUpdatedBy: string,
    DtLastUpdated?: Date,
    Student: IStudent,
    Campus: ICampus,
    ReferralGroup: IReferralGroup,
    ReferralType: IReferralType,
    ReferralReasons: IReferralReason
}