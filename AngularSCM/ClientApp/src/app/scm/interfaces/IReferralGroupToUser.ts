import { IISDStaff } from "../../shared/interfaces/IISDStaff";
import { IDriver } from "./IDriver";
import { ITeacher } from "./ITeacher";

export interface IReferralGroupToUser{
    Id: number,
    ReferralGroupId: number,
    UserId: number,
    UserType: number,
    Created: Date,
    Updated?: Date,
    ReferralGroup: any,
    Staff: IISDStaff,
    Driver: IDriver,
    Teacher: ITeacher
}

export interface IReferralGroupToUserData {
    Id : number,
    ReferralGroupId: number,
    UserId: number,
    UserType: number,
}