import { IStudent } from "./IStudent";
import { ICampus } from "./ICampus";
import { IBus } from "./IBus";
import { IParent } from "./IParent";
import { IDriver } from './IDriver'

export interface IIncidentReport {
    Id: number,
    StudentId: number,
    CampusId: number,
    BusId: number, 
    DateOfIncident: Date,
    ParentId: number,
    DateOfReport: Date,
    Grade: string,
    TimeOfDay: number,
    ParentPhoneNumber: string,
    DriverId: number,
    Driver: IDriver,
    PanicButtonUsed: number,
    PulledVideo: number,
    DescriptionOfStudent: string,
    IncidentStatus: number,
    IsActive: boolean,
    IncidentNotes: string,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    Student: IStudent,
    Campus: ICampus,
    Parent: IParent,
    Bus: IBus,
    CampusName: string,
    LastName: string,
    FirstName: string,
    BusName: string,
    ParentName: string,
    DriverName: string
}