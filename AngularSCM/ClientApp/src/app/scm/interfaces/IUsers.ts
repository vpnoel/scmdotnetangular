export interface IUsers {
    Id: number,
    UserName: string,
    Password: string,
    Source: string,
    SourceId: number,
    CreatedBy: number,
    DtCreated: Date,
    LastUpdatedBy?: number,
    DtLastUpdated?: Date,
}