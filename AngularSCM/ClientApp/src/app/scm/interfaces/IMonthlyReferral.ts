export interface IMonthlyReferral {
    MonthName: string,
    ReferralCount: number
}