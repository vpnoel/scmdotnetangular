export interface IReferralType {
    ReferralTypeId: number,
    ReferralType1: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date
}