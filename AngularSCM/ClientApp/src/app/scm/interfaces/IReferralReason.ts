import { IReferralType } from "./IReferralType";

export interface IReferralReason {
    Id: number;
    ReferralReason: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    ReferralTypeId?: number,
}