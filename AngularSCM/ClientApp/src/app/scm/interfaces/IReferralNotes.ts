import { IReferralTransaction } from "./IReferralTransaction";
import { IReferralGroup} from "./IReferralGroup";

export interface IReferralNotes {
    Id: number,
    ReferralTransactionId: number,
    ReferralGroupId: number,
    ReviewerId: number,
    Notes: string,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    ReferralTransaction: IReferralTransaction,
    ReferralGroup: IReferralGroup
}