export interface IISD {
    ISDId: number,
    ISDName: string,
}