export interface IAgingReport {
    CampusName: string,
    ReferralGroup: string,
    CDays: number,
    TSDays: number,
    ETDays: number,
    TTDays: number,
    TotalCount: number
}