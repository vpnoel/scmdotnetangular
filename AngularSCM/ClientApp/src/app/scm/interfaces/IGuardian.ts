export interface IGuardian {
    GuardianId: number,
    GuardianName: string,
    Relationship: string
}