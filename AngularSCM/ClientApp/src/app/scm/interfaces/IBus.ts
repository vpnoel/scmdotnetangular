export interface IBus {
    BusId: number,
    LicensePlateNumber: string,
    VIN: string,
    BusName: string
}