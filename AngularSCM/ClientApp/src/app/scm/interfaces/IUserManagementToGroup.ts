import { IUserPermissionGroup } from "./IUserPermissionGroup";
import { IISDStaff} from "./IISDStaff";

export interface IUserManagementToGroup {
    Id: number,
    UserPermissionGroupId: number,
    UserId: number,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated: Date,
    UpdatedBy: number,
    DtLastUpdated: Date,
    UserPermissionGroup: IUserPermissionGroup,
    ISDStaff: IISDStaff
}