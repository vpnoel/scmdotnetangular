export interface IParent {
    ParentId: number,
    ParentName: string,
    PrimaryCellNumber: string
}