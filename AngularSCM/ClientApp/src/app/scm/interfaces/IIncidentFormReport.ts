export interface IIncidentFormReport {
    CampusName: string,
    TotalCount: number
}