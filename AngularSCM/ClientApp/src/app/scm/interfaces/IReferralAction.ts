export interface IReferralAction {
    Id: number,
    ReferralAction: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    ReferralTypeId: number
}