import { IISD } from "./IISD";

export interface IUserPermissionGroup {
    Id: number,
    GroupName: string,
    IsActive: boolean,
    ISDId: number,
    CreatedBy: number,
    DtCreated: Date,
    UpdatedBy: number,
    DtLastUpdated: Date,
    ISD: IISD
}