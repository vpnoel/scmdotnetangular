import { IReferralType } from "./IReferralType";
import { IReferralGroup } from "./IReferralGroup";

export interface IReferralTypeGroup {
    Id: number,
    ReferralTypeId: number,
    ReferralGroupId: number,
    IsAssociated: boolean,
    Created: number,
    DtCreated: Date,
    ReferralType: IReferralType,
    ReferralGroup: IReferralGroup
}

export interface IReferralTypeGroupMatrixData {
    ReferralTypes: IReferralType,
    ReferraltypeGroupAssociate: IReferralTypeGroup[]
}