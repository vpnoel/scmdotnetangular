export interface IReferralReasonReport {
    ReferralReasons: string,
    TotalCount: number
}