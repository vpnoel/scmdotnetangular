export interface ICampus {
    CampusId: number,
    CampusExternalId: string,
    CampusName: string,
    ISDId: number,
    CreatedBy: string,
    DtCreated?: Date
    LastUpdatedBy: string,
    DtLastUpdated?: Date,
    SchoolLogoUrl: string,
    IsActive: number,
    WithCDModule: number
}