import { ICampus } from "./ICampus";
import { IISD } from "./IISD";

export interface IISDStaff {
    ISDStaffId: number,
    LoginName: string,
    FirstName: string,
    LastName: string,
    EmailAddress: string,
    Password: string,
    Active: number,
    ISDId: number,
    IsAdmin: boolean,
    CampusId: number
}