import { IISDStaff } from "./IISDStaff";
import { IISD } from "./IISD";
import {ICampus} from "./ICampus";

export interface IReferralGroup {
    Id: number,
    ReferralGroup: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    IsAdmin: boolean,
    ISDId: number,
    CampusId: number,
    ISD: IISD,
    ISDStaff: IISDStaff,
    Campus: ICampus

}