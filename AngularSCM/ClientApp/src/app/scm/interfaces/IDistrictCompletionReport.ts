export interface IDistrictCompletionReport {
    CampusName: string,
    ReferralTotal: number,
    AveDays: number,
    MinDays: number,
    MaxDays: number
}