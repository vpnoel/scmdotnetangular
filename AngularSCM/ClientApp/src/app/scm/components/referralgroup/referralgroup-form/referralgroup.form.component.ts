import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: "referralgroup-form",
    templateUrl: './referralgroup.form.component.html',
    styleUrls: [
        './referralgroup.form.component.scss'
    ]
})

export class ReferralGroupFormComponent implements OnInit {
    constructor(private router: Router) { }

    ngOnInit(): void {
    }

    close(): void {
        this.router.navigate(["/main/referralgroup"]);
    }
}