import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, ViewEncapsulation, Inject, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import {MatPaginatorModule} from '@angular/material/paginator';

import { NgbModal, NgbTooltip} from '@ng-bootstrap/ng-bootstrap';

//Services
import { ReferralActionService } from '../../services/referralaction.services';
import { ReferralReasonService} from '../../services/referralreason.services';
import { DataService } from './../../../shared/services/data.service';

//Interfaces
import { IReferralType } from '../../interfaces/IReferralType';

// Material Data
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { ReferralReasonModalComponent } from '../modal/referralreasonmodal/referralreasonmodal.component';

//Enum
import { FeaturesEnum} from './../../enum/FeaturesEnum';
import { ISDTypeEnum } from '../../enum/ISDTypeEnums';
import { IReferralReason } from '../../interfaces/IReferralReason';
import { IReferralAction } from '../../interfaces/IReferralAction';
import { ReferralActionModalComponent } from '../modal/referralactionmodal/referralactionmodal.component';

@Component({
    selector: "setup",
    templateUrl: './setup.component.html',
    styleUrls: [
        './setup.component.scss'
    ]
})

export class SetupComponent implements OnInit {

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild('TABLE', {static: true}) table: ElementRef;

    displayedColumns = ['ReferralReason', 'ReferralType', 'IsActive', 'Actions', 'Delete'];
    displayedColumns2 = ['ReferralAction', 'ReferralType', 'IsActive', 'Actions', 'Delete'];

    ELEMENT_DATA_RR: IReferralReason[];
    ELEMENT_DATA_RA: IReferralAction[];

    dataSource;
    dataSourceRA;

    // NG MODEL
    rReason: string = "";
    rAction: string = "";
    rType: string ="";
    isActive: boolean = true;
    buttonDisabled: boolean = true;
    show: boolean = true; 

    private isButtonVisible = true;

    mode: string;

    activeList: any = [
        {
            "name": "Active",
            "value": true
        },
        {
            "name": "Inactive",
            "value": false
        }
    ];

    constructor(
        public dataService: DataService,
        public router: Router,
        private modalService: NgbModal,
        public dialog: MatDialog,
        public snackbar: MatSnackBar,
        private changeDetectorRefs: ChangeDetectorRef,
        private _referralReasonService: ReferralReasonService,
        private _referralActionService: ReferralActionService
        ) {
    }

    ngOnInit(): void {

        this.getDataSourceRR();

        this.getDataSourceRA();
    }

    private getDataSourceRR(): void {
        this._referralReasonService.getReferralReasonRecords()
            .subscribe(res => {
                console.log(res);
                this.ELEMENT_DATA_RR = res;
                this.dataSource = new MatTableDataSource<IReferralReason>(this.ELEMENT_DATA_RR);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.changeDetectorRefs.detectChanges();
            });
    }

    private getDataSourceRA(): void {
        this._referralActionService.getReferralActionRecords()
            .subscribe(res => {
                console.log(res);
                this.ELEMENT_DATA_RA = res;
                this.dataSourceRA = new MatTableDataSource<IReferralAction>(this.ELEMENT_DATA_RA);
                this.dataSourceRA.paginator = this.paginator;
                this.dataSourceRA.sort = this.sort;
                this.changeDetectorRefs.detectChanges();
            });
    }

    mapActiveStatusData(id: number): string {
        let retData = "";
        retData = this.activeList.find(e => e.value === id);
        return retData;
    }

    applyFilterRR(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    applyFilterRA(filterValue: string) {
        this.dataSourceRA.filter = filterValue.trim().toLowerCase();

        if (this.dataSourceRA.paginator) {
            this.dataSourceRA.paginator.firstPage();
        }
    }

    openDialogRR(opt: string, data: any): void {
        const dialogConfig = new MatDialogConfig();
            dialogConfig.data = { mode: opt, item: data };
            dialogConfig.width = "500px";
            dialogConfig.height = "380px";
            const dialogRef = this.dialog.open(ReferralReasonModalComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(res => {
                this.refresh_RR();
            });
    }

    openDialogRA(opt: string, data: any): void {
        const dialogConfig = new MatDialogConfig();
            dialogConfig.data = { mode: opt, item: data };
            dialogConfig.width = "500px";
            dialogConfig.height = "380px";
            const dialogRef = this.dialog.open(ReferralActionModalComponent, dialogConfig);
            dialogRef.afterClosed().subscribe(res => {
                this.refresh_RA();
            });
    }

    // openDialog(opt: string, data: any): void {
    //     let loggedUserData = JSON.parse(localStorage.loggedUser);

    //     let addReferraTypeFeature = (loggedUserData.Permission !== null) ? loggedUserData.Permission.find(el => el.RoleFeaturesId === FeaturesEnum.AddReferralType) : undefined;
    //     let editReferralTypeFeature = (loggedUserData.Permission !== null) ? loggedUserData.Permission.find(el => el.RoleFeaturesId === FeaturesEnum.EditReferralType) : undefined;

    //     if (loggedUserData.ISDId === ISDTypeEnum.SuperAdmin) {
    //         const dialogConfig = new MatDialogConfig();
    //         dialogConfig.data = { mode: opt, item: data };
    //         dialogConfig.width = "650px";
    //         dialogConfig.height = "300px";
    //         const dialogRef = this.dialog.open(ReferralReasonModalComponent, dialogConfig);
    //         dialogRef.afterClosed().subscribe(res => {
    //             this.refresh_RR();
    //         });
    //     }
    //     else if(addReferraTypeFeature != undefined && addReferraTypeFeature.IsActive == true && editReferralTypeFeature != undefined && editReferralTypeFeature.IsActive == true){
    //         const dialogConfig = new MatDialogConfig();
    //         dialogConfig.data = { mode: opt, item: data };
    //         dialogConfig.width = "650px";
    //         dialogConfig.height = "300px";
    //         const dialogRef = this.dialog.open(ReferralReasonModalComponent, dialogConfig);
    //         dialogRef.afterClosed().subscribe(res => {
    //             this.refresh_RR();
    //         });
    //     }else{
    //         this.snackbar.open('You have no permission to Add/Edit Referral Type!', '', {
    //             duration: 3000,
    //         });
    //     }
        
    // }

    private refreshTableRR() {
        // if there's a paginator active we're using it for refresh
        if (this.dataSource.paginator.hasNextPage()) {
            this.dataSource.paginator.nextPage();
            this.dataSource.paginator.previousPage();
            // in case we're on last page this if will tick
        } else {
            this.dataSource.paginator.hasPreviousPage()
            this.dataSource.paginator.previousPage();
            this.dataSource.paginator.nextPage();
        }
    }

    private refreshTableRA() {
        // if there's a paginator active we're using it for refresh
        if (this.dataSourceRA.paginator.hasNextPage()) {
            this.dataSourceRA.paginator.nextPage();
            this.dataSourceRA.paginator.previousPage();
            // in case we're on last page this if will tick
        } else {
            this.dataSourceRA.paginator.hasPreviousPage()
            this.dataSourceRA.paginator.previousPage();
            this.dataSourceRA.paginator.nextPage();
        }
    }

    refresh_RR(){
       this._referralReasonService.getReferralReasonRecords()
            .subscribe(res => {
                this.ELEMENT_DATA_RR = res;
                this.dataSource = new MatTableDataSource<IReferralReason>(this.ELEMENT_DATA_RR);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.changeDetectorRefs.detectChanges();
            });

        this.refreshTableRR();
    }

    refresh_RA(){
        this._referralActionService.getReferralActionRecords()
             .subscribe(res => {
                 this.ELEMENT_DATA_RA = res;
                 this.dataSourceRA = new MatTableDataSource<IReferralAction>(this.ELEMENT_DATA_RA);
                 this.dataSourceRA.paginator = this.paginator;
                 this.dataSourceRA.sort = this.sort;
                 this.changeDetectorRefs.detectChanges();
             });
 
         this.refreshTableRA();
     }

}