import { Component, Inject, OnInit } from '@angular/core';

@Component({
    selector: "student-form",
    templateUrl: './student.form.component.html',
    styleUrls: [
        './student.form.component.scss'
    ]
})

export class StudentFormComponent implements OnInit {
    constructor() { }

    ngOnInit(): void {
    }
}