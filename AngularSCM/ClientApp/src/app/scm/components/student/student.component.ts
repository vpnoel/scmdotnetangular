import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, ViewEncapsulation, Inject, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

//Services
import { StudentService } from '../../services/student.services';
import { DataService } from './../../../shared/services/data.service';

// Material Data
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';

//Interfaces
import { IStudent } from '../../interfaces/IStudent';

@Component({
    selector: "student",
    templateUrl: './student.component.html',
    styleUrls: [
        './student.component.scss'
    ]
})

export class StudentComponent implements OnInit {
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild('TABLE', {static: true}) table: ElementRef;

    displayedColumns = ['Student', 'StudentId', 'CampusName', 'Grade', 'DateOfBirth', 'View', 'Create'];

    ELEMENT_DATA: IStudent[];

    dataSource;

    // NG MODEL

    mode: string;

    constructor(
        public dataService: DataService,
        public router: Router,
        public dialog: MatDialog,
        public snackbar: MatSnackBar,
        private changeDetectorRefs: ChangeDetectorRef,
        private _studentService: StudentService
    ) { }

    ngOnInit(): void {
        this.getDataSource();
    }

    private getDataSource(): void {
        this._studentService.getStudentRecord()
            .subscribe(res => {
                console.log(res);
                this.ELEMENT_DATA = res;
                this.dataSource = new MatTableDataSource<IStudent>(this.ELEMENT_DATA);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.changeDetectorRefs.detectChanges();
            });
    }

    private refreshTable() {
        // if there's a paginator active we're using it for refresh
        if (this.dataSource.paginator.hasNextPage()) {
            this.dataSource.paginator.nextPage();
            this.dataSource.paginator.previousPage();
            // in case we're on last page this if will tick
        } else {
            this.dataSource.paginator.hasPreviousPage()
            this.dataSource.paginator.previousPage();
            this.dataSource.paginator.nextPage();
        }
    }

    refresh(){
        this._studentService.getStudentRecord()
             .subscribe(res => {
                 this.ELEMENT_DATA = res;
                 this.dataSource = new MatTableDataSource<IStudent>(this.ELEMENT_DATA);
                 this.dataSource.paginator = this.paginator;
                 this.dataSource.sort = this.sort;
                 this.changeDetectorRefs.detectChanges();
             });
 
         this.refreshTable();
     }
}