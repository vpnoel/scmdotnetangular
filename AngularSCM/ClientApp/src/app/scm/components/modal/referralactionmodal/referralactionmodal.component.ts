import { Component, OnInit, Inject } from "@angular/core";
import { Router } from '@angular/router';
import { ModalService } from "../../../../shared/services/modal.services";
import { ReferralActionService } from "../../../services/referralaction.services";
import { ReferralTypeService} from "../../../services/referraltype.services";
import { IReferralAction } from "../../../interfaces/IReferralAction";

// Material
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { forkJoin } from "rxjs";

@Component({
    selector: 'referralaction-modal',
    templateUrl: './referralactionmodal.component.html',
    styleUrls: [
        './referralactionmodal.component.scss'
    ]
})
export class ReferralActionModalComponent implements OnInit {

    mode: string = "";
    referralAction: any[] = [];
    referralTypeList: any[] = [];

    errors: string[] = [];

    referralActionObject: IReferralAction = {
        CreatedBy: 0,
        DtCreated: undefined,
        DtLastUpdated: undefined,
        ReferralAction: '',
        IsActive: true,
        LastUpdatedBy: 0,
        Id: 0,
        ReferralTypeId: 0
    }

    // NG MODEL
    rAction: string = "";
    rTypeId: number;
    rType: string = "";
    isActive: boolean = true;
    currentId: number;

    isValid: boolean = false;
    createdBy: number;
    lastUpdatedBy: number;
    currentLoggedStaffId: number;

    constructor(
        public dialogRef: MatDialogRef<ReferralActionModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public snackbar: MatSnackBar,
        private router: Router,
        private _referralActionService: ReferralActionService,
        private _referralTypeService: ReferralTypeService) {
        
        let loggedUserdata = JSON.parse(localStorage.loggedUser);

        this.isValidFormData();
    }

    ngOnInit(): void {
        this.mode = this.data.mode;

        if (this.mode === "Edit") {
            let editableData = this.data.item;

            this.referralActionObject = {
                CreatedBy: editableData.CreatedBy,
                DtCreated: editableData.DtCreated,
                DtLastUpdated: editableData.DtLastUpdated,
                ReferralAction: editableData.ReferralAction,
                IsActive: editableData.IsActive,
                LastUpdatedBy: editableData.LastUpdatedBy,
                Id: editableData.Id,
                ReferralTypeId: editableData.ReferralTypeId
            }
        }

        this.initializeFormData();
    }

    initializeFormData(): void {

        let data = JSON.parse(localStorage.loggedUser);

        forkJoin(
            this._referralTypeService.getReferralTypeRecords(),
            this._referralActionService.getReferralActionRecords()
        ).subscribe(([rtr, rrr]) => {
            this.referralTypeList = rtr;
            this.referralAction = rrr;
        })
    }

    isValidFormData(): boolean {
        return (this.referralActionObject.ReferralAction !== '' && this.referralActionObject.ReferralTypeId !== 0) ? true : false;
    }

    saveChanges(): void {
        let loggedUser = JSON.parse(localStorage.loggedUser);

        this.referralActionObject.CreatedBy = loggedUser.ISDStaffId;
        this.referralActionObject.LastUpdatedBy = loggedUser.ISDStaffId;

        if (this.isValidFormData()) {
            if (this.mode === 'Edit')
            {
                this._referralActionService.updateReferralActionRecords(this.referralActionObject.Id, this.referralActionObject)
                .subscribe(res => {
                    this.snackbar.open('Successfully Updated!', '', {
                        duration: 2000,
                    });
                }, err => {
                    if(err.status === 400) {
                        let validationErrorDictionary = JSON.parse(err.text());
                        for(var fieldname in validationErrorDictionary){
                            if(validationErrorDictionary.hasOwnProperty(fieldname)){
                                this.errors.push(validationErrorDictionary[fieldname]);
                            }
                        }
                    } else {
                        this.errors.push("something went wrong");
                    }
                });
            }
            else{
                this._referralActionService.addReferralActionRecords(this.referralActionObject)
                    .subscribe(res => { 
                        this.snackbar.open('Successfully Saved!', '', {
                            duration: 2000,
                        }); 
                    });
            }
            this.close();
        }else{
            this.snackbar.open('Please fill the required fields!', '', {
                duration: 2000,
            });
        }
    }

    close(): void {
        this.dialogRef.close();
        this.router.navigate(['/main/setup']);
    }
}