import { Component, OnInit, Inject } from "@angular/core";
import { Router } from '@angular/router';
import { ModalService } from "../../../../shared/services/modal.services";
import { ReferralReasonService } from "../../../services/referralreason.services";
import { ReferralTypeService} from "../../../services/referraltype.services";
import { IReferralReason } from "../../../interfaces/IReferralReason";

// Material
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { forkJoin } from "rxjs";

@Component({
    selector: 'referralreason-modal',
    templateUrl: './referralreasonmodal.component.html',
    styleUrls: [
        './referralreasonmodal.component.scss'
    ]
})
export class ReferralReasonModalComponent implements OnInit {

    mode: string = "";
    referralReason: any[] = [];
    referralTypeList: any[] = [];

    referralReasonObject: IReferralReason = {
        CreatedBy: 0,
        DtCreated: undefined,
        DtLastUpdated: undefined,
        ReferralReason: '',
        IsActive: true,
        LastUpdatedBy: 0,
        ReferralTypeId: 0,
        Id:0
    }

    // NG MODEL
    rReason: string = "";
    rTypeId: number;
    rType: string = "";
    isActive: boolean = true;
    currentId: number;

    isValid: boolean = false;
    createdBy: number;
    lastUpdatedBy: number;
    currentLoggedStaffId: number;

    constructor(
        public dialogRef: MatDialogRef<ReferralReasonModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public snackbar: MatSnackBar,
        private router: Router,
        private _referralReasonService: ReferralReasonService,
        private _referralTypeService: ReferralTypeService) {
        
        let loggedUserdata = JSON.parse(localStorage.loggedUser);
        
        this.isValidFormData();
    }

    ngOnInit(): void {
        this.mode = this.data.mode;

        if (this.mode === "Edit") {
            let editableData = this.data.item;
            
            this.referralReasonObject = {
                CreatedBy: editableData.CreatedBy,
                DtCreated: editableData.DtCreated,
                DtLastUpdated: editableData.DtLastUpdated,
                ReferralReason: editableData.ReferralReason,
                IsActive: editableData.IsActive,
                LastUpdatedBy: editableData.LastUpdatedBy,
                Id: editableData.Id,
                ReferralTypeId: editableData.ReferralTypeId
            }
        }

        this.initializeFormData();
    }

    initializeFormData(): void {

        let data = JSON.parse(localStorage.loggedUser);

        forkJoin(
            this._referralTypeService.getReferralTypeRecords(),
            this._referralReasonService.getReferralReasonRecords()
        ).subscribe(([rtr, rrr]) => {
            console.log(rtr);
            this.referralTypeList = rtr;
            this.referralReason = rrr;
        })
    }

    isValidFormData(): boolean {
        return (this.referralReasonObject.ReferralReason !== '' && this.referralReasonObject.ReferralTypeId !== 0) ? true : false;
    }

    saveChanges(): void {
        let loggedUser = JSON.parse(localStorage.loggedUser);

        this.referralReasonObject.CreatedBy = loggedUser.ISDStaffId;
        this.referralReasonObject.LastUpdatedBy = loggedUser.ISDStaffId;

        if (this.isValidFormData()) {
            
            if (this.mode === 'Edit')
            {
                this._referralReasonService.updateReferralReasonRecords(this.referralReasonObject.Id, this.referralReasonObject)
                .subscribe(res => {
                    this.snackbar.open('Successfully Updated!', '', {
                        duration: 2000,
                    });
                });
            }
            else{
                this._referralReasonService.addReferralReasonRecords(this.referralReasonObject)
                    .subscribe(res => { 
                        this.snackbar.open('Successfully Saved!', '', {
                            duration: 2000,
                        }); 
                    });
            }
            this.close();
        }else{
            this.snackbar.open('Please fill the required fields!', '', {
                duration: 2000,
            });
        }
    }

    close(): void {
        this.dialogRef.close();
        this.router.navigate(['/main/setup']);
    }
}