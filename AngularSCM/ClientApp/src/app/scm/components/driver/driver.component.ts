import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, ViewEncapsulation, Inject, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

//Services
import { DriverService } from '../../services/driver.services';
import { DataService } from './../../../shared/services/data.service';

// Material Data
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';

//Interfaces
import { IDriver } from '../../interfaces/IDriver';

@Component({
    selector: "driver",
    templateUrl: './driver.component.html',
    styleUrls: [
        './driver.component.scss'
    ]
})

export class DriverComponent implements OnInit {

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild('TABLE', {static: true}) table: ElementRef;

    displayedColumns = ['DriverName', 'PhoneNumber', 'Actions'];

    ELEMENT_DATA: IDriver[];

    dataSource;

    // NG MODEL
    driverName: string = "";
    phoneNumber: string = "";
    referrals: number;

    mode: string;

    constructor(
        public dataService: DataService,
        public router: Router,
        public dialog: MatDialog,
        public snackbar: MatSnackBar,
        private changeDetectorRefs: ChangeDetectorRef,
        private _driverService: DriverService
    ) { }

    ngOnInit(): void {
        this.getDataSource();
    }

    private getDataSource(): void {
        this._driverService.getDriverecords()
            .subscribe(res => {
                console.log(res);
                this.ELEMENT_DATA = res;
                this.dataSource = new MatTableDataSource<IDriver>(this.ELEMENT_DATA);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.changeDetectorRefs.detectChanges();
            });
    }

    private refreshTable() {
        // if there's a paginator active we're using it for refresh
        if (this.dataSource.paginator.hasNextPage()) {
            this.dataSource.paginator.nextPage();
            this.dataSource.paginator.previousPage();
            // in case we're on last page this if will tick
        } else {
            this.dataSource.paginator.hasPreviousPage()
            this.dataSource.paginator.previousPage();
            this.dataSource.paginator.nextPage();
        }
    }

    refresh(){
        this._driverService.getDriverecords()
             .subscribe(res => {
                 this.ELEMENT_DATA = res;
                 this.dataSource = new MatTableDataSource<IDriver>(this.ELEMENT_DATA);
                 this.dataSource.paginator = this.paginator;
                 this.dataSource.sort = this.sort;
                 this.changeDetectorRefs.detectChanges();
             });
 
         this.refreshTable();
     }
}