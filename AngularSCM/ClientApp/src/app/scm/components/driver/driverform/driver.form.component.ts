import { Component, Inject, OnInit } from '@angular/core';

@Component({
    selector: "driver-form",
    templateUrl: './driver.form.component.html',
    styleUrls: [
        './driver.form.component.scss'
    ]
})

export class DriverFormComponent implements OnInit {
    constructor() { }

    ngOnInit(): void {
    }
}