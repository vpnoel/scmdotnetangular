import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ISDTypeEnum } from '../../enum/ISDTypeEnums';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { FeaturesEnum } from '../../enum/FeaturesEnum';

@Component({
    selector: "login-form",
    templateUrl: './login.component.html',
    styleUrls: [
        './login.component.scss'
    ]
})

export class LoginFormComponent implements OnInit{

    isDriver: boolean = false;
    idText: string = "Username";
    pwText: string = "Password"
    username: string ="";
    password: string ="";

    form: FormGroup;
    private formSubmitAttempt: boolean;

    constructor(
        private fb: FormBuilder,
        private authService: AuthService,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            userName: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    isFieldInvalid(field: string) {
        return (
            (!this.form.get(field).valid && this.form.get(field).touched) ||
            (this.form.get(field).untouched && this.formSubmitAttempt)
        );
    }

    switch() {
        this.isDriver = !this.isDriver;
        this.idText = (this.isDriver) ? "Employee Id" : "Username";
        this.pwText = (this.isDriver) ? "Pin Code" : "Password";
    }

    onSubmit() {
        if (this.form.valid) {
            var authService = this.authService.login(this.form.value).subscribe(res => {
                if (res !== null) {
                    // if (res.isdStaffId !== 0) {
                    if (res.ISDStaff !== null) {

                        let object: any = res.ISDStaff;
                        object.Permission = res.Permissions;
                        localStorage.setItem("loggedUser", JSON.stringify(object));
                        let loggedUserData = JSON.parse(localStorage.loggedUser);

                        let getViewDashboardFeature = (loggedUserData.Permission !== null) ? loggedUserData.Permission.find(el => el.RoleFeaturesId === FeaturesEnum.ViewDashboard) : undefined;
                        let getViewReferralFeature = (loggedUserData.Permission !== null) ? loggedUserData.Permission.find(el => el.RoleFeaturesId === FeaturesEnum.ViewReferralTransaction) : undefined;

                        if(loggedUserData.ISDId === ISDTypeEnum.SuperAdmin){
                            this.router.navigate(['/main/dashboard']);
                        }else if(getViewDashboardFeature != undefined && getViewDashboardFeature.IsActive == true){
                            this.router.navigate(['/main/dashboard']);
                        }
                        else if(getViewReferralFeature != undefined && getViewReferralFeature.IsActive == true){
                            this.router.navigate(['/main/referraltransaction']);
                        }else{
                            this.router.navigate(['/404']);
                        }

                        localStorage.setItem("loggedUser", JSON.stringify(object));
                        
                    }
                    else if(res.Driver != null){
                        this.router.navigate(['/main/driver/referral']);
                        let object: any = res.Driver;
                        object.Permission = null;
                        localStorage.setItem('loggedUser', JSON.stringify(object));
                    }
                    else if(res.Teacher != null){
                        //this.router.navigate(['/main/driver/referral']);
                        let object: any = res.Teacher;
                        object.Permission = null;
                        localStorage.setItem('loggedUser', JSON.stringify(object));
                    }
                } 

            });
        }
        this.formSubmitAttempt = true;
    }
}