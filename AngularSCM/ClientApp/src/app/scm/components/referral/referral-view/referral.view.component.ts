import { Component, Inject, OnInit } from '@angular/core';
import { NgbDatepicker, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: "referral-view",
    templateUrl: './referral.view.component.html',
    styleUrls: [
        './referral.view.component.scss'
    ]
})

export class ReferralViewComponent implements OnInit {

    date: Date;
    endDate: Date;
    bsValue = new Date();
    userResults: any;

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        const date: NgbDateStruct = { year: 1789, month: 7, day: 14 }; // July, 14 1789

        this.userResults = [
            { id: 0, displayName: "Austin Taylor" },
            { id: 1, displayName: "Tom Cruise" },
            { id: 2, displayName: "Scarlett Johannsen" },
            { id: 3, displayName: "Marilyn Brando" },
            { id: 4, displayName: "Jennifer Love Hewitt" },
        ]
    }

    viewStudent(): void{
        this.router.navigate(["/main/student/view/1"]);
    }

    viewDriver(): void{
        this.router.navigate(["/main/driver/view/1"]);
    }

    editReferral(): void {
        this.router.navigate(["/main/referral/edit/1"]);
    }
}