// Angular Core Component
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { NgbDatepicker, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

// Services
import { ReferralTransactionService } from '../../services/referraltransaction.services';
import { DataService } from './../../../shared/services/data.service';

// Material Data
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';

// Interfaces
import { IReferralTransaction } from '../../interfaces/IReferralTransaction';

// Enums
import { FeaturesEnum} from './../../enum/FeaturesEnum';

@Component({
    selector: "referral",
    templateUrl: './referral.component.html',
    styleUrls: [
        './referral.component.scss'
    ]
})

export class ReferralComponent implements OnInit {

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild('TABLE', {static: true}) table: ElementRef;

    displayedColumns = ['Date', 'Student', 'ReferralType', 'Campus', 'Driver', 'Route', 'Status', 'View','Edit', 'Delete'];

    ELEMENT_DATA: IReferralTransaction[];

    dataSource;

    startDate: Date;
    endDate: Date;
    campusId: number;

    statusList: any = [
        {
            "name": "Not Approved",
            "value": 1
        },
        {
            "name": "Action Required",
            "value": 2
        },
        {
            "name": "Completed",
            "value": 3
        },
    ];

    constructor(public dialog: MatDialog,
        public dataService: DataService,
        public snackbar: MatSnackBar,
        public router: Router,
        public route: ActivatedRoute,
        private _referralTransactionService: ReferralTransactionService) {
    }

    ngOnInit(): void {
        const date: NgbDateStruct = { year: 1789, month: 7, day: 14 }; // July, 14 1789

        this.getDataSource();
    }

    private getDataSource(): void {
        this._referralTransactionService.getReferralRecords()
                .subscribe(res => {
                    this.ELEMENT_DATA = res;
                    this.dataSource = new MatTableDataSource<IReferralTransaction>(this.ELEMENT_DATA);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                });
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    mapStatusData(id: number): string {
        let retData = "";
        retData = this.statusList.find(e => e.value === id);
        return retData;
    }

    private refreshTable() {
        this.getDataSource();
        // if there's a paginator active we're using it for refresh
        if (this.dataSource.paginator.hasNextPage()) {
            this.dataSource.paginator.nextPage();
            this.dataSource.paginator.previousPage();
            // in case we're on last page this if will tick
        } else {
            this.dataSource.paginator.hasPreviousPage()
            this.dataSource.paginator.previousPage();
            this.dataSource.paginator.nextPage();
        }
    }
}