import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDatepicker, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { ReferralSteps } from 'src/app/scm/enum/ReferralStepsEnum';
import { IReferralTransaction } from 'src/app/scm/interfaces/IReferralTransaction';
import { IReferralReason } from 'src/app/scm/interfaces/IReferralReason';
import { ReferralReasonService } from 'src/app/scm/services/referralreason.services';
import { referralTransactionDefault } from 'src/app/shared/helpers/default.helpers';
import { StudentService } from 'src/app/scm/services/student.services';

@Component({
    selector: "referral-form",
    templateUrl: './referral.form.component.html',
    styleUrls: [
        './referral.form.component.scss'
    ]
})

export class ReferralFormComponent implements OnInit {

    date: Date;
    endDate: Date;
    bsValue = new Date();
    
    
    // Form Related Model
    referralTransactionObject: IReferralTransaction = referralTransactionDefault();
    studentInformationModel: any = {
        FirstName: "",
        LastName: "",
        Id: ""
    };

    // Selection
    referralReason: IReferralReason[] = [];
    studentResults: any;

    // States
    stateBullet: number = 1;
    step = ReferralSteps;

    constructor(
        private router: Router, 
        private referralReasonService: ReferralReasonService,
        private studentService: StudentService) {
    }

    ngOnInit(): void {
        const date: NgbDateStruct = { year: 1789, month: 7, day: 14 }; // July, 14 1789

        this.studentResults = [
            { Id: 0, DisplayName: "Domo Incar" },
            { Id: 1, DisplayName: "Sefi Roth" },
            { Id: 2, DisplayName: "Rath Skar" },
        ]

        this.referralReasonService.getReferralReasonRecords()
        .subscribe(res => {
            this.referralReason = res;
        });
    }

    close(): void {
        this.router.navigate(["/main/referral"]);
    }
    
    isActive(state): boolean {
        return this.stateBullet === state;
    }

    next(): void {
        // Reset and Remove previous datasets
        this.studentResults = null;
        if(this.stateBullet < 4) {
            if(this.stateBullet == 1) {
                if(this.referralTransactionObject.DateOfIncident !== null && this.referralTransactionObject.ReferralTypeId !== 0) 
                {
                    this.stateBullet++;
                }
                else {
                    alert("Please complete all required fields on this screen.");
                }
            }
            else {
                this.stateBullet++;
            }
        }
    }

    create(): void{}

    searchStudentInformation(): void {
        this.studentService.searchStudentByInformation(this.studentInformationModel)
        .subscribe(res => {
            this.studentResults = res;
        });
    }

    previous(): void {
        if(this.stateBullet > 1) this.stateBullet--;
    }

    handleSelectedStudent(selected: any): void {
        console.log(selected);
    }
}