import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDatepicker, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: "referral-edit",
    templateUrl: './referral.edit.component.html',
    styleUrls: [
        './referral.edit.component.scss'
    ]
})

export class ReferralEditComponent implements OnInit {

    date: Date;
    endDate: Date;
    bsValue = new Date();
    stateBullet: number = 1;

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        const date: NgbDateStruct = { year: 1789, month: 7, day: 14 }; // July, 14 1789
    }

    close(): void {
        this.router.navigate(["/main/referral"]);
    }
}