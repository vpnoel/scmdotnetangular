import { Component, NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import * as CanvasJS from 'canvasjs';
import { single } from '../../../shared/data';

@Component({
    selector: "dashboard",
    templateUrl: './dashboard.component.html',
    styleUrls: [
        './dashboard.component.scss'
    ]
})

export class DashboardComponent implements OnInit{

    single: any[];
      
    multi: any[];
  
    view: any[] = [540, 280];
  
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'Status';
    showYAxisLabel = true;
    yAxisLabel = 'No. of Referrals';
  
    colorScheme = {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };
  
    constructor() {
      Object.assign(this, { single })
    }
  
    onSelect(event) {
      console.log(event);
    }

    ngOnInit(): void {
      
    }
}