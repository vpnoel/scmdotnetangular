import { Component, Inject, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService } from '../../../shared/services/auth.service';
import { routerTransition } from './../../../shared/router.animation';

@Component({
    selector: "main-board",
    templateUrl: './main.board.component.html',
    styleUrls: [
        './main.board.component.scss'
    ],
    animations: [ routerTransition() ]
})

export class MainBoardComponent implements OnInit {
    menuModel = [
        {
          title: 'Dashboard',
          routerUrl: '/main/dashboard',
        },
        {
          title: 'Referal',
          routerUrl: '/main/referral',
        },
        {
          title: 'Student',
          routerUrl: '/main/student',
        },
        {
          title: 'Reports',
          routerUrl: '/main/report',
        },
        {
          title: 'Campuses',
          routerUrl: '/main/campus',
        },
        {
          title: 'Drivers',
          routerUrl: '/main/driver',
        },
        {
          title: 'Conduct Staff',
          routerUrl: '/main/conductstaff',
        },
        {
          title: 'Referral Groups',
          routerUrl: '/main/referralgroup',
        },
        {
          title: 'Setup',
          routerUrl: '/main/setup',
        },
      ];

    constructor(
      private router: Router,
      private authService: AuthService
      ) { }

    getState(outlet) {
        return this.router.url;
      }
    
    ngOnInit(): void {
    }

    onLogout() {
      localStorage.clear()
      this.authService.logout();
  }
}