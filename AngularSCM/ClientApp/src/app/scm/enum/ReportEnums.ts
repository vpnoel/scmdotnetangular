export enum Reports {
    Aging = 'aging',
    ReferralReason = 'referralreason',
    District = 'district',
    Incident = 'incident'
}