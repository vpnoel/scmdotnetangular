export enum ReferralTransaction {
    Student = 'student',
    Teacher = 'teacher',
    Driver = 'driver',
    Campus = 'campus',
    Guardian = 'guardian',
    Parent = 'parent'
}