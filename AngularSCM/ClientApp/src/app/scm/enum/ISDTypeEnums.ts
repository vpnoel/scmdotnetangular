export enum ISDTypeEnum {
    Development = 1000,
    CampusAdministrator = 1001,
    BusSupervisor = 1002,
    SuperAdmin = 1003,
    DevOnProduction = 1111,
    AliefInDev = 1281
}