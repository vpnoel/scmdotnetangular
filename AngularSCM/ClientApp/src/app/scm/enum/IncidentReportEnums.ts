export enum IncidentReport {
    Student = 'student',
    Bus = 'bus',
    Driver = 'driver',
    Campus = 'campus',
    Parent = 'parent'
}