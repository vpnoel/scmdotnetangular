
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralActionGroup } from "../interfaces/IReferralActionGroup";


@Injectable()
export class ReferralActionGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralActionGroupAssociate`;

    constructor(private http: HttpClientExt) { }

    addReferralActionGroupRecords(referralActionGroup: IReferralActionGroup): Observable<IReferralActionGroup> {
        return this.http.post(this.controllerApi, referralActionGroup).pipe(
            map(res => res as IReferralActionGroup));
    }

    getReferralActionGroupAssociateByActionId(id: number): Observable<IReferralActionGroup[]> {
        const url = `${this.controllerApi}/getReferralActionGroupByActionId/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralActionGroup[] || [];
            }));
    }

    //ReferralAction Group Associate
    getReferralActionGroupAssociateByGroupId(id: number): Observable<IReferralActionGroup[]> {
        const url = `${this.controllerApi}/getReferralActionGroupAssociate/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralActionGroup[] || [];
            }));
    }

    upsertReferralActionGroupAssociate(items: IReferralActionGroup[]): Observable<IReferralActionGroup[]> {
        const url = `${this.controllerApi}/upsertReferralActionGroupAssociate`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IReferralActionGroup[] || [];
            }))
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralActionGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}