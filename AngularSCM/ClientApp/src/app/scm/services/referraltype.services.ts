
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralType } from "../interfaces/IReferralType";
import { IReferralTypeGroup, IReferralTypeGroupMatrixData} from "../interfaces/IReferralTypeGroup";


@Injectable()
export class ReferralTypeService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralTypes`;
    //private controllerApi = '/api/ReferralTypes'

    constructor(private http: HttpClientExt) { }

    addReferralTypeRecords(referralType: IReferralType): Observable<IReferralType> {
        return this.http.post(this.controllerApi, referralType).pipe(
            map(res => res as IReferralType));
    }

    getReferralTypeById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralTypeDetail = res as any || {};
                return referralTypeDetail;
            }));
    }

    getReferralTypeRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralTypeDetails = res as any || {};
                return referralTypeDetails;
            }));
    }

    updateReferralTypeRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    getReferralTypeByReasonId(referralReasonId: number): Observable<any>{
        const url = `${this.controllerApi}`;

        var data = {
            "ReferralReasonsId": referralReasonId
        }
        return this.http.post(`${url}/LookUpAndFindByReferralReasonId`, data).pipe(
            map(res => {
                const referralTypeDetails = res as any[] || [];
                return referralTypeDetails
            }));


    }

    getReferralTypeByGroupId(referralGroupId: number): Observable<any>{
        const url = `${this.controllerApi}`;

        var data = {
            "ReferralGroupId": referralGroupId
        }
        return this.http.post(`${url}/LookUpAndFindByReferralGroupId`, data).pipe(
            map(res => {
                const referralTypeGroupDetails = res as any[] || [];
                return referralTypeGroupDetails
            }));


    }

    getReferralTypeGroupAssociateMatrixData(): Observable<IReferralTypeGroupMatrixData[]>{
        const url =`${AppSettings.SITE_HOST}/api/ReferralTypeGroupAssociate/getMatrixReferralTypeGroupAssociates`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralTypeGroupMatrixData[] || [];
            }))
    }

    getReferralTypeGroupAssociate(id: number): Observable<IReferralTypeGroup[]> {
        const url = `${this.controllerApi}/getReferralTypeGroupAssociate/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralTypeGroup[] || [];
            }));
    }

    upsertReferralTypeGroupAssociate(items: IReferralTypeGroup[]): Observable<IReferralTypeGroup[]> {
        const url = `${this.controllerApi}/upsertReferralTypeGroupAssociate`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IReferralTypeGroup[] || [];
            }))
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralTypeService: ${error}`);
        return observableThrowError(error.message || error);
    }

}