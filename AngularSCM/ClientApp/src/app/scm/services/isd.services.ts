
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class ISDService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Isds`;
    //private controllerApi = '/api/ISD'

    constructor(private http: HttpClientExt) { }

    getISDRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const isdDetails = res as any[] || [];
                return isdDetails;
            }));
    }

    getISDRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const isdDetail = res as any || {};
                return isdDetail;
            }));
    }

    getISDRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const isdDetails = res as any || {};
                return isdDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ISDService: ${error}`);
        return observableThrowError(error.message || error);
    }

}