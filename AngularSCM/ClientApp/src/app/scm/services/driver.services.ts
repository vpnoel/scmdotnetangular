
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class DriverService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Drivers`;
    //private controllerApi = '/api/Drivers'

    constructor(private http: HttpClientExt) { }

    getDriverRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const driverDetails = res as any[] || [];
                return driverDetails;
            }));
    }


    getDriverRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const driverDetails = res as any || {};
                return driverDetails;
            }));
    }

    getDriverecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const driverDetails = res as any || {};
                return driverDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`DriverService: ${error}`);
        return observableThrowError(error.message || error);
    }

}