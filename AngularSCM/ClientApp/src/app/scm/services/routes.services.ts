
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class RoutesService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Routes`;
    //private controllerApi = '/api/Routes'

    constructor(private http: HttpClientExt) { }


    getRoutesById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const routeDetails = res as any || {};
                return routeDetails;
            }));
    }

    GetRoutesRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const routeDetails = res as any || {};
                return routeDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`CampusService: ${error}`);
        return observableThrowError(error.message || error);
    }

}