
import {throwError as observableThrowError,  Observable, ObservableLike } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class StudentService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Students`;
    //private controllerApi = '/api/Students'

    constructor(private http: HttpClientExt) { }


    getStudentRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const studentDetails = res as any[] || [];
                return studentDetails;
            }));
    }

    searchStudentByInformation(studentInfo: any): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "FirstName": studentInfo.FirstName,
            "LastName": studentInfo.LastName,
            "StudentId": studentInfo.Id
        }
        return this.http.post(`${url}/LookupByMixedItems`, data).pipe(
            map(res => {
                const studentDetails = res as any[] || [];
                return studentDetails
            }))
    }

    getStudentRecordQueryByCampusId(queryString: string, campusId: number): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": "",
            "CampusId": campusId
        }
        return this.http.post(`${url}/LookUpAndFindByCampusId`, data).pipe(
            map(res => {
                const studentDetails = res as any[] || [];
                return studentDetails
            }))
    }

    getStudentRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const studentDetail = res as any || {};
                return studentDetail;
            }));
    }

    getStudentRecord(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const studentDetails = res as any || {};
                return studentDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralTransactionService: ${error}`);
        return observableThrowError(error.message || error);
    }

}