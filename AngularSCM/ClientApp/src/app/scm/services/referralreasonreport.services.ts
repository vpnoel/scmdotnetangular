
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralReasonReport } from "../interfaces/IReferralReasonReport";


@Injectable()
export class ReferralReasonReportService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Reports`;
    //private controllerApi = '/api/Reports'

    constructor(private http: HttpClientExt) { }

    getReferralReasonReportRecords(): Observable<any> {
        const url = `${this.controllerApi}/RRReport`;

        return this.http.get(url).pipe(
            map(res => {
                const referralReasonReportDetails = res as any || {};
                return referralReasonReportDetails
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralReasonReport: ${error}`);
        return observableThrowError(error.message || error);
    }

}