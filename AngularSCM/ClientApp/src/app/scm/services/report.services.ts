
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralReasonReport } from "../interfaces/IReferralReasonReport";
import { IAgingReport } from "../interfaces/IAgingReport";


@Injectable()
export class ReportService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Reports`;
    //private controllerApi = '/api/Reports'

    constructor(private http: HttpClientExt) { }

    getAgingReportsQuery(startDate: any, endDate: any, campusId: number): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": campusId,
        }
        return this.http.post(`${url}/AgingReportQuery`, data).pipe(
            map(res => {
                const agingReportDetails = res as any[] || [];
                return agingReportDetails;
            }));
    }

    getReferralReasonReportQuery(startDate: any, endDate: any, campusId: number): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": campusId,
        }
        return this.http.post(`${url}/RRReportQuery`, data).pipe(
            map(res => {
                const referralReasonReportDetails = res as any[] || [];
                return referralReasonReportDetails;
            }));
    }

    getDistrictCompletionReportQuery(startDate: any, endDate: any, campusId: number): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": campusId,
        }
        return this.http.post(`${url}/DCReportQuery`, data).pipe(
            map(res => {
                const districtCompletionReportDetails = res as any[] || [];
                return districtCompletionReportDetails;
            }));
    }

    getIncidentFormReportQuery(startDate: any, endDate: any): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "StartDate": startDate,
            "EndDate": endDate
        }
        return this.http.post(`${url}/IRReportQuery`, data).pipe(
            map(res => {
                const incidentFormReportDetails = res as any[] || [];
                return incidentFormReportDetails;
            }));
    }

    getReferralReasonReportRecords(): Observable<any> {
        const url = `${this.controllerApi}/RRReport`;

        return this.http.get(url).pipe(
            map(res => {
                const referralReasonReportDetails = res as any || {};
                return referralReasonReportDetails
            }));
    }

    getAgingReportRecords(): Observable<any> {
        const url = `${this.controllerApi}/AgingReport`;

        return this.http.get(url).pipe(
            map(res => {
                const agingReportDetails = res as any || {};
                return agingReportDetails
            }));
    }

    getDistrictCompletionReportRecords(): Observable<any> {
        const url = `${this.controllerApi}/DCReport`;

        return this.http.get(url).pipe(
            map(res => {
                const districtCompletionReportDetails = res as any || {};
                return districtCompletionReportDetails
            }));
    }

    getIncidentFormReportRecords(): Observable<any> {
        const url = `${this.controllerApi}/IncidentFormReport`;

        return this.http.get(url).pipe(
            map(res => {
                const incidentFormReportDetails = res as any || {};
                return incidentFormReportDetails
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`Report: ${error}`);
        return observableThrowError(error.message || error);
    }

}