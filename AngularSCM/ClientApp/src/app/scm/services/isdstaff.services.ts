
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IISDStaff } from "../interfaces/IISDStaff";


@Injectable()
export class ISDStaffService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Isdstaff`;
    //private controllerApi = '/api/ISDStaff'

    constructor(private http: HttpClientExt) { }

    getISDStaffRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const isdStaffDetails = res as any[] || [];
                return isdStaffDetails;
            }));
    }

    getISDStaffById(id: number): Observable<any[]> {
        const url = `${this.controllerApi}/${id}`;
        return this.http.get(url).pipe(
            map( res => {
                const isdStaff = res as any || {};
                return isdStaff;
            }))
    }

    getISDStaffByCampusId(campusId: number): Observable<any> {
        const url = `${this.controllerApi}`;
        return this.http.get(`${url}/Lookup/${campusId}`).pipe(
            map(res => {
                const isdStaffDetails = res || {};
                return isdStaffDetails;
            }));
    }

    getISDStaffRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const isdStaffDetails = res as any || {};
                return isdStaffDetails;
            }));
    }

    getByIsdIdAndCampusId(isdId: number, campusId: number): Observable<any[]>{
        const url = `${this.controllerApi}/GetByIsdIdAndCampusId/${isdId}/${campusId}`;

        return this.http.get(url).pipe(
            map(res=>{
                const isdStaffDetails = res as any[] || [];
                return isdStaffDetails;
            }));
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ISDStaffService: ${error}`);
        return observableThrowError(error.message || error);
    }

}