
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralTypeGroup } from "../interfaces/IReferralTypeGroup";


@Injectable()
export class ReferralTypeGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralTypeGroupAssociate`;

    constructor(private http: HttpClientExt) { }

    addReferralTypeGroupRecords(referralTypeGroup: IReferralTypeGroup): Observable<IReferralTypeGroup> {
        return this.http.post(this.controllerApi, referralTypeGroup).pipe(
            map(res => res as IReferralTypeGroup));
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralTypeGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}