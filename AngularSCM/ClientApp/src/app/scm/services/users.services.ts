
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IUsers } from "../interfaces/IUsers";


@Injectable()
export class UsersService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Users`;
    //private controllerApi = '/api/Users'

    constructor(private http: HttpClientExt) { }

    getUsersRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const usersDetails = res as any || {};
                return usersDetails;
            }));
    }
    
    getUsersRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const usersDetail = res as any || {};
                return usersDetail;
            }));
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`UsersService: ${error}`);
        return observableThrowError(error.message || error);
    }

}