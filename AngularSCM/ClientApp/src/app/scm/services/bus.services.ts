
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class BusService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Buses`;
    //private controllerApi = '/api/Bus'

    constructor(private http: HttpClientExt) { }

    getBusRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const busDetails = res as any[] || [];
                return busDetails;
            }));
    }


    getBusRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const busDetail = res as any || {};
                return busDetail;
            }));
    }

    getBusRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const busDetails = res as any || {};
                return busDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`BusService: ${error}`);
        return observableThrowError(error.message || error);
    }

}
