
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IUserPermission } from "../interfaces/IUserPermission";


@Injectable()
export class SettingsService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Settings`;

    constructor(private http: HttpClientExt) { }

    getSettingsByCategoryId(id: number): Observable<any[]> {
        const url = `${this.controllerApi}/ByCategory/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const settings = res as any[] || [];
                return settings;
            }));
    }

    getAllSettings(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const settings = res as any[] || [];
                return settings;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`SettingService: ${error}`);
        return observableThrowError(error.message || error);
    }

}