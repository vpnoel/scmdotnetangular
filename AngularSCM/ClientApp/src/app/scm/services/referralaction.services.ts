
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralAction } from "../interfaces/IReferralAction";
import { IReferralActionGroup, IReferralActionGroupMatrixData} from "../interfaces/IReferralActionGroup";


@Injectable()
export class ReferralActionService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralActions`;
    //private controllerApi = '/api/ReferralActions'

    constructor(private http: HttpClientExt) { }

    addReferralActionRecords(referralActions: IReferralAction): Observable<IReferralAction> {
        return this.http.post(this.controllerApi, referralActions).pipe(
            map(res => res as IReferralAction));
    }

    getReferralActionById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralActionDetail = res as any || {};
                return referralActionDetail;
            }));
    }

    getReferralActionRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralActionDetails = res as any || {};
                return referralActionDetails;
            }));
    }

    updateReferralActionRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    getReferralActionGroupAssociateMatrixData(): Observable<IReferralActionGroupMatrixData[]>{
        const url =`${AppSettings.SITE_HOST}/api/ReferralActionGroupAssociate/getMatrixReferralActionGroupAssociates`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralActionGroupMatrixData[] || [];
            }))
    }

    getReferralActionGroupAssociate(id: number): Observable<IReferralActionGroup[]> {
        const url = `${this.controllerApi}/getReferralActionGroupAssociate/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralActionGroup[] || [];
            }));
    }


    upsertReferralActionGroupAssociate(items: IReferralActionGroup[]): Observable<IReferralActionGroup[]> {
        const url = `${this.controllerApi}/upsertReferralActionGroupAssociate`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IReferralActionGroup[] || [];
            }))
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralActionService: ${error}`);
        return observableThrowError(error.message || error);
    }

}