
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IUserManagementToGroup } from "../interfaces/IUserManagementToGroup";


@Injectable()
export class UserManagementToGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/UserManagementToGroup`;

    constructor(private http: HttpClientExt) { }

    addUserManagementToGroup(userManagementToGroup: any[]): Observable<IUserManagementToGroup[]>{
        return this.http.post(this.controllerApi, userManagementToGroup).pipe(
            map(res => {
                return res as IUserManagementToGroup[] || []
            }));
    }

    getUserManagementToGroupById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const userManagementToGroupDetail = res as any || {};
                return userManagementToGroupDetail;
            }));
    }

    getMembersByUserGroup(id: number): Observable<any> {
        const url = `${this.controllerApi}/userManagementByGroup/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const userManagementToGroupDetail = res as any || {};
                return userManagementToGroupDetail;
            }));
    }

    getUserManagementToGroupRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const userManagementToGroupDetails = res as any || {};
                return userManagementToGroupDetails;
            }));
    }

    updateUserManagementToGroupGroup(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    upsertUserManagementToGroup(items: IUserManagementToGroup[]): Observable<IUserManagementToGroup[]> {
        const url = `${this.controllerApi}/upsertUserManagementToGroup`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IUserManagementToGroup[] || [];
            }))
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`IUserManagementToGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}