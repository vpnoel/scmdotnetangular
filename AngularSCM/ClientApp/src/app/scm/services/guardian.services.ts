
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class GuardianService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Guardians`;
    //private controllerApi = '/api/Guardians'

    constructor(private http: HttpClientExt) { }

    getGuardianRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const driverDetails = res as any[] || [];
                return driverDetails;
            }));
    }

    getGuardianRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const guardianDetail = res as any || {};
                return guardianDetail;
            }));
    }

    getGuardianRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const guardianDetails = res as any || {};
                return guardianDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`GuardianService: ${error}`);
        return observableThrowError(error.message || error);
    }

}