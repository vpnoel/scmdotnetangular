
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class ParentService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Parents`;
    //private controllerApi = '/api/Parent'

    constructor(private http: HttpClientExt) { }

    getParentRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const parentDetails = res as any[] || [];
                return parentDetails;
            }));
    }


    getParentRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const parentDetail = res as any || {};
                return parentDetail;
            }));
    }

    getParentRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const parentDetails = res as any || {};
                return parentDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ParentService: ${error}`);
        return observableThrowError(error.message || error);
    }

}