
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IEmailTransaction } from "../interfaces/IEmailTransaction";

@Injectable()
export class EmailTransactionService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/EmailTransactions`;
    //private controllerApi = '/api/EmailTransaction'

    constructor(private http: HttpClientExt) { }

    addEmailRecords(emailTransaction: IEmailTransaction): Observable<IEmailTransaction> {
        return this.http.post(this.controllerApi, emailTransaction).pipe(
            map(res => {
                return res as IEmailTransaction
            }));
    }

    getEmailTransactionById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const EmailTransactionDetails = res as any || {};
                return EmailTransactionDetails;
            }));
    }

    addEmailTransaction(data: IEmailTransaction): Observable<any> {
        const url = `${this.controllerApi}/SendEmail`;

        return this.http.post(`${url}`, data).pipe(
            map(res => {
                return res as IEmailTransaction;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`EmailTransactionService: ${error}`);
        return observableThrowError(error.message || error);
    }

}