
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralReason } from "../interfaces/IReferralReason";
import { IReferralReasonGroup, IReferralReasonGroupMatrixData} from "../interfaces/IReferralReasonGroup";


@Injectable()
export class ReferralReasonService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralReasons`;
    //private controllerApi = '/api/ReferralReasons'

    constructor(private http: HttpClientExt) { }

    addReferralReasonRecords(referralReason: IReferralReason): Observable<IReferralReason> {
        return this.http.post(this.controllerApi, referralReason).pipe(
            map(res => res as IReferralReason));
    }

    getReferralReasonById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralReasonDetail = res as any || {};
                return referralReasonDetail;
            }));
    }

    getReferralReasonRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralReasonDetails = res as any || {};
                return referralReasonDetails;
            }));
    }

    updateReferralReasonRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    getReferralReasonByGroupId(referralGroupId: number): Observable<any>{
        const url = `${this.controllerApi}`;

        var data = {
            "ReferralGroupId": referralGroupId
        }
        return this.http.post(`${url}/LookUpAndFindByReferralGroupId`, data).pipe(
            map(res => {
                const referralReasonDetails = res as any[] || [];
                return referralReasonDetails
            }));


    }

    getReferralReasonGroupAssociateMatrixData(): Observable<IReferralReasonGroupMatrixData[]>{
        const url =`${AppSettings.SITE_HOST}/api/ReferralReasonGroupAssociate/getMatrixReferralReasonGroupAssociates`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralReasonGroupMatrixData[] || [];
            }))
    }

    getReferralReasonGroupAssociate(id: number): Observable<IReferralReasonGroup[]> {
        const url = `${this.controllerApi}/getReferralReasonGroupAssociate/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralReasonGroup[] || [];
            }));
    }

    getReferralReasonByTypeId(referralTypeId: number): Observable<any>{
        const url = `${this.controllerApi}`;

        var data = {
            "ReferralTypeId": referralTypeId
        }
        return this.http.post(`${url}/LookUpAndFindByReferralTypeId`, data).pipe(
            map(res => {
                const referralReasonsDetails = res as any[] || [];
                return referralReasonsDetails
            }));


    }

    upsertReferralReasonGroupAssociate(items: IReferralReasonGroup[]): Observable<IReferralReasonGroup[]> {
        const url = `${this.controllerApi}/upsertReferralReasonGroupAssociate`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IReferralReasonGroup[] || [];
            }))
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralReasonService: ${error}`);
        return observableThrowError(error.message || error);
    }

}