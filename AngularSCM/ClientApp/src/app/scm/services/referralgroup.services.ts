
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralGroup } from "../interfaces/IReferralGroup";
import { IReferralGroupToUser, IReferralGroupToUserData } from "../interfaces/IReferralGroupToUser";
import { IReferralActionGroup } from "../interfaces/IReferralActionGroup";
import { IReferralReasonGroup} from "../interfaces/IReferralReasonGroup";


@Injectable()
export class ReferralGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralGroups`;
    //private controllerApi = '/api/ReferralGroups'

    constructor(private http: HttpClientExt) { }

    addReferralGroupRecords(referralGroup: IReferralGroup): Observable<IReferralGroup> {
        return this.http.post(this.controllerApi, referralGroup).pipe(
            map(res => res as IReferralGroup));
    }

    getReferralGroupById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralGroupDetail = res as any || {};
                return referralGroupDetail;
            }));
    }

    getReferralGroupRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralGroupDetails = res as any || {};
                return referralGroupDetails;
            }));
    }

    updateReferralGroupRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
            
    }

    getReferralGroupMembers(referralGroupId: number): Observable<IReferralGroupToUser[]> {
        const url = `${AppSettings.SITE_HOST}/api/ReferralGroupToUser/byReferralGroup/${referralGroupId}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralGroupToUsers = res as IReferralGroupToUser[] || []
                return referralGroupToUsers;
            }))
    }

    updateReferralGroupMembers(referralGroupId: number, items: IReferralGroupToUserData[]): Observable<IReferralGroupToUser[]> {
        const url = `${AppSettings.SITE_HOST}/api/ReferralGroupToUser/byReferralGroup/${referralGroupId}`; 

        return this.http.put(url, items).pipe(
            map(res => {
                const result = res as IReferralGroupToUser[] || [];
                return result;
            }))
    }

    removeReferralGroupMembers(referralGroupId: number, ids: number[]): Observable<IReferralGroupToUser[]> {
        const url = `${AppSettings.SITE_HOST}/api/ReferralGroupToUser/removeItems/${referralGroupId}`;  

        return this.http.post(url, ids).pipe(
            map(res => {
                const result = res as IReferralGroupToUser[] || [];
                return result;
            }));
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}