
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IUserPermission } from "../interfaces/IUserPermission";


@Injectable()
export class UserPermissionService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/UserPermissions`;

    constructor(private http: HttpClientExt) { }

    addUserPermission(userPermission: any[]): Observable<IUserPermission[]>{
        return this.http.post(this.controllerApi, userPermission).pipe(
            map(res => {
                return res as IUserPermission[] || []
            }));
    }

    getUserPermissionById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const userPermissionDetail = res as any || {};
                return userPermissionDetail;
            }));
    }

    getUserPermissionByReferralGroup(id:number): Observable<any>{
        const url = `${this.controllerApi}/byReferralGroup/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const userPermissionsDetail = res as any || {};
                return userPermissionsDetail;
            }));
    }

    getUserPermissionRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const userPermissionDetails = res as any || {};
                return userPermissionDetails;
            }));
    }

    updateUserPermission(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    getUserPermissionReferralGroup(id: number): Observable<IUserPermission[]> {
        const url = `${this.controllerApi}/getUserPermissionByReferralGroup/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IUserPermission[] || [];
            }));
    }

    getUserPermissionByUserGroup(id: number): Observable<IUserPermission[]> {
        const url = `${this.controllerApi}/getUserPermissionByUserGroup/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IUserPermission[] || [];
            }))
    }

    upsertUserPermissionRecords(referralGroupId: number, items: IUserPermission[]): Observable<IUserPermission[]> {
        const url = `${this.controllerApi}/upsertUserPermission/${referralGroupId}`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IUserPermission[] || [];
            }))
    }

    upsertUserPermissions(items: IUserPermission[]): Observable<IUserPermission[]> {
        const url = `${this.controllerApi}/upsertUserPermission`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IUserPermission[] || [];
            }))
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`UserPermissionService: ${error}`);
        return observableThrowError(error.message || error);
    }

}