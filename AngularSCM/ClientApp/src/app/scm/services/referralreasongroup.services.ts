
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralReasonGroup } from "../interfaces/IReferralReasonGroup";


@Injectable()
export class ReferralReasonGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralReasonGroupAssociate`;

    constructor(private http: HttpClientExt) { }

    addReferralReasonGroupRecords(referralReasonGroup: IReferralReasonGroup): Observable<IReferralReasonGroup> {
        return this.http.post(this.controllerApi, referralReasonGroup).pipe(
            map(res => res as IReferralReasonGroup));
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralReasonGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}