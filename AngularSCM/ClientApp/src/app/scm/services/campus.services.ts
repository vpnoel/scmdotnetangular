
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class CampusService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Campus`;
    //private controllerApi = '/api/Campus'

    constructor(private http: HttpClientExt) { }

    getCampusRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const campusDetails = res as any[] || [];
                return campusDetails;
            }));
    }


    getCampusRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const campusDetail = res as any || {};
                return campusDetail;
            }));
    }

    getCampusRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const campusDetails = res as any || {};
                return campusDetails;
            }));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`CampusService: ${error}`);
        return observableThrowError(error.message || error);
    }

}
