
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IDashboard } from "../interfaces/IDashboard";
import { IMonthlyReferral } from "../interfaces/IMonthlyReferral";
import { IReferralReasonReport } from "../interfaces/IReferralReasonReport";


@Injectable()
export class DashboardService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Dashboard`;
    //private controllerApi = '/api/Dashboard'

    constructor(private http: HttpClientExt) { }

    getMonthlyReferralRecords(): Observable<any> {
        const url = `${this.controllerApi}/MRReport`;

        return this.http.get(url).pipe(
            map(res => {
                const monthlyReferralReportDetails = res as any || {};
                return monthlyReferralReportDetails
            }));
    }

    getMonthlyReferralRecordsByCampusId(campusId: number): Observable<any> {
        const url = `${this.controllerApi}/MRReport/${campusId}`;

        return this.http.get(url).pipe(
            map(res => {
                const monthlyReferralReportDetails = res as any || {};
                return monthlyReferralReportDetails
            }));
    }

    getDashboardReferralReasonRecords(): Observable<any> {
        const url = `${this.controllerApi}/RRReport`;

        return this.http.get(url).pipe(
            map(res => {
                const referralReasonReportDetails = res as any || {};
                return referralReasonReportDetails
            }));
    }

    addReferralRecords(dashboard: IDashboard): Observable<IDashboard> {
        return this.http.post(this.controllerApi, dashboard).pipe(
            map(res => {
                return res as IDashboard
            }));
    }

    getReferralRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralTransactionDetail = res as any || {};
                return referralTransactionDetail;
            }));
    }

    getReferralRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralTransactionDetails = res as any || {};
                return referralTransactionDetails
            }));
    }

    updateReferralRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`DashboardService: ${error}`);
        return observableThrowError(error.message || error);
    }

}