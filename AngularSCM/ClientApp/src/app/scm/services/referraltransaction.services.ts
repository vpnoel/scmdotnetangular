
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralTransaction, CustomReferralTransaction } from "../interfaces/IReferralTransaction";
import { IReferralTransactionReasons } from "../interfaces/IReferralTransactionReason";


@Injectable()
export class ReferralTransactionService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralTransactions`;
    //private controllerApi = '/api/ReferralTransactions'

    constructor(private http: HttpClientExt) { }

    addDriverReferralRecords(referralTransactionModel: CustomReferralTransaction): Observable<CustomReferralTransaction>{
        return this.http.post(this.controllerApi, referralTransactionModel).pipe(
            map(res => {
                return res as any;
            }));
    }

    addReferralRecords(referralTransactionModel: CustomReferralTransaction): Observable<CustomReferralTransaction> {
        return this.http.post(this.controllerApi, referralTransactionModel)
            .pipe(
                map(res => {
                    return res as any;
                })
            );
    }

    addReferralTransactionRecords(referralTransaction: IReferralTransaction): Observable<IReferralTransaction>{
        return this.http.post(this.controllerApi, referralTransaction).pipe(
            map(res => {
                return res as IReferralTransaction
            }));
    }


    getReferralRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralTransactionDetail = res as any || {};
                return referralTransactionDetail;
            }));
    }

    getReferralRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralTransactionDetails = res as any || {};
                return referralTransactionDetails;
            }));
    }

    getReferralRecordByDate(startDate: any, endDate: any, campusId: number): Observable<any> {

        let logged = JSON.parse(localStorage.loggedUser);
        let userId = logged.ISDStaffId;

        const url = `${this.controllerApi}/SortByDate/${userId}`;

        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": campusId, // Not required
        }

        return this.http.post(`${url}`, data).pipe(
            map(res => {
                const referralTransactionDetails = res as any[] || [];
                return referralTransactionDetails;
            }));
    }

    getDriverReferralByDate(startDate: any, endDate: any, campusId:number): Observable<any> {
        let logged = JSON.parse(localStorage.loggedUser);
        let userId = logged.ISDStaffId;

        const url = `${this.controllerApi}/SortDriverByDate/${userId}`;

        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": campusId, // Not required
        }

        return this.http.post(`${url}`, data).pipe(
            map(res => {
                const driverReferralDetails = res as any[] || [];
                return driverReferralDetails;
            }));
    }

    getReferralReasonByGroupId(referralGroupId: number){
        const url = `${this.controllerApi}/ByReferralGroupId/${referralGroupId}`;

        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralReasonByGroup = res as any || {};
                return referralReasonByGroup;
            }))
    }

    getReferralReasonByGroup(referralGroupId: number){
        const url = `${this.controllerApi}/GetReferralReasonsByGroup/${referralGroupId}`;

        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralReasonByGroups = res as any || {};
                return referralReasonByGroups;
            }))
    }

    getReferralTransactionReasons(id: number): Observable<IReferralTransactionReasons[]> {
        const url = `${this.controllerApi}/getReferralTransactionReasons/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                return res as IReferralTransactionReasons[] || [];
            }));
    }

    getByReferralId(studentId: number): Observable<any>{
        return this.http.get(`${this.controllerApi}/ByReferralId/${studentId}`).pipe(
            map(res => {
                const referralTransactionReasonsRecords = res as any [] || [];
                return referralTransactionReasonsRecords
            }))
    }

    upsertReferralTransactionReasons(referralTransactionId : number, items: IReferralTransactionReasons[]): Observable<IReferralTransactionReasons[]> {
        const url = `${this.controllerApi}/upsertReferralTransactionReasons/${referralTransactionId}`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IReferralTransactionReasons[] || [];
            }))
    }

    getReferralRecordAdmin(startDate: any, endDate: any, campusId:number): Observable<any> {
        const url = `${this.controllerApi}/SortByDate`;

        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": 0, // Not required
        }

        return this.http.post(`${url}`, data).pipe(
            map(res => {
                const referralTransactionDetails = res as any[] || [];
                return referralTransactionDetails;
            }));
    }

    getReferralRecordsByCampusId(campusId: number): Observable<any> {
        return this.http.get(`${this.controllerApi}/ByCampus/${campusId}`).pipe(
            map(res => {
                const referralTransactionRecords = res as any || {};
                return referralTransactionRecords;
            }));
    }

    getReferralRecordsByStudentId(studentId: number): Observable<any> {
        return this.http.get(`${this.controllerApi}/ByStudent/${studentId}`).pipe(
            map(res => {
                const referralTransactionStudentRecords = res as any [] || [];
                return referralTransactionStudentRecords
            }))
    }

    getReferralRecordHistory(studentId: number): Observable<any>{
        return this.http.get(`${this.controllerApi}/ReferralHistoryBy/${studentId}`).pipe(
            map(res => {
                const referralTransactionHistoryRecords = res as any [] || [];
                return referralTransactionHistoryRecords
            }));
    }

    getReferralNotesData(referralId: number): Observable<any>{
        return this.http.get(`${this.controllerApi}/notesByReferral/${referralId}`).pipe(
            map(res => {
                const referralNotesData = res as any [] || [];
                return referralNotesData
            }));
    }

    getReferralNotes(id: number): Observable<any>{
        return this.http.get(`${this.controllerApi}/getReferralTransactionNotes/${id}`).pipe(
            map(res => {
                const referralNotesRecords = res as any [] || [];
                return referralNotesRecords
            }));
    }

    updateAllReferralRecords(id: number, data: CustomReferralTransaction): Observable<CustomReferralTransaction> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as CustomReferralTransaction
            ));
    }

    updateReferralRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any
            ));
    }

    escalateReferralRecords(id: number, data: any): Observable<any>{
        const url = `${this.controllerApi}/escalateBy/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    reviewReferralRecords(id: number, data: any): Observable<any>{
        const url = `${this.controllerApi}/review/${id}`;

        return this.http.put(url, data).pipe(
            map(res => 
                res as any || {}
                ));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralTransactionService: ${error}`);
        return observableThrowError(error.message || error);
    }

}