
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IIncidentReport } from "../interfaces/IIncidentReport";


@Injectable()
export class IncidentReportService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/IncidentReports`;
    //private controllerApi = '/api/IncidentReport'

    constructor(private http: HttpClientExt) { }

    addIncidentRecords(incidentReport : IIncidentReport): Observable<IIncidentReport> {
        return this.http.post(this.controllerApi, incidentReport).pipe(
            map(res => {
                return res as IIncidentReport
            }));
    }

    getIncidentRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const incidentReportDetail = res as any || {};
                return incidentReportDetail;
            }));
    }

    getIncidentRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const incidentReportDetails = res as any || {};
                return incidentReportDetails;
            }));
    }

    getIncidentRecordByDate(startDate: any, endDate: any): Observable<any> {
        const url = `${this.controllerApi}/SortByDate`;

        var data = {
            "StartDate": startDate,
            "EndDate": endDate,
            "CampusId": 0, // Not required
        }

        return this.http.post(`${url}`, data).pipe(
            map(res => {
                const incidentReportDetails = res as any[] || [];
                return incidentReportDetails;
            }));
    }

    getIncidentRecordsByCampusId(campusId: number): Observable<any> {
        return this.http.get(`${this.controllerApi}/ByCampus/${campusId}`).pipe(
            map(res => {
                const incidentReportRecords = res as any || {};
                return incidentReportRecords;
            }));
    }

    getIncidentRecordsByStudentId(studentId: number): Observable<any> {
        return this.http.get(`${this.controllerApi}/ByStudent/${studentId}`).pipe(
            map(res => {
                const incidentReportStudentRecords = res as any[] || [];
                return incidentReportStudentRecords
            }));
    }

    getIncidentRecordsByStatus(incidentStatus: number): Observable<any> {
        return this.http.get(`${this.controllerApi}/SortByStatus/${incidentStatus}`).pipe(
            map(res => {
                const incidentReportStatusRecords = res as any[] || [];
                return incidentReportStatusRecords
            }));
    }

    updateIncidentRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    updateOpenIncidentRecords(id: number, data:any): Observable<any> {
        const url = `${this.controllerApi}/changeStatus/${id}`;

        return this.http.put(url, data).pipe(
            map(res => 
                res as any || {}
            ));
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`IncidentReportService: ${error}`);
        return observableThrowError(error.message || error);
    }

}