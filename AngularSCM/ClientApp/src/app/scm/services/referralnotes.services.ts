
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralNotes } from '../interfaces/IReferralNotes';


@Injectable()
export class ReferralNotesService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralNotes`;
    //private controllerApi = '/api/ReferralNotes'

    constructor(private http: HttpClientExt) { }

    addReferralNotesRecords(referralNotes: IReferralNotes): Observable<IReferralNotes> {
        return this.http.post(this.controllerApi, referralNotes).pipe(
            map(res => res as IReferralNotes));
    }

    getReferralNotesById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralNotesDetail = res as any || {};
                return referralNotesDetail;
            }));
    }

    getReferralNotesByReferralTransaction(id: number): Observable<IReferralNotes[]> {
        const url = `${this.controllerApi}/byReferral/${id}`;
        return this.http.get(url).pipe(
            map(res => {
                const referralNotesDetails = res as IReferralNotes[] || [];
                return referralNotesDetails
            })
        );
    }

    getReferralNotesRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralNotesDetails = res as any || {};
                return referralNotesDetails;
            }));
    }

    upsertReferralNotes(referralTransactionId : number, items: IReferralNotes[]): Observable<IReferralNotes[]> {
        const url = `${this.controllerApi}/upsertReferralNotes/${referralTransactionId}`;

        return this.http.post(`${url}`, items).pipe(
            map(res => {
                return res as IReferralNotes[] || [];
            }))
    }

    updateReferralNotesRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
            
    }


    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`ReferralNotesService: ${error}`);
        return observableThrowError(error.message || error);
    }

}