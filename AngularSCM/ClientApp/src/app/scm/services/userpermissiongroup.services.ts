
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IUserPermissionGroup } from "../interfaces/IUserPermissionGroup";


@Injectable()
export class UserPermissionGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/UserPermissionGroup`;

    constructor(private http: HttpClientExt) { }

    addUserPermissionGroup(userPermissionGroup: IUserPermissionGroup): Observable<IUserPermissionGroup>{
        return this.http.post(this.controllerApi, userPermissionGroup).pipe(
            map(res => res as IUserPermissionGroup));
    }

    getUserPermissionGroupById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const userPermissionGroupDetail = res as any || {};
                return userPermissionGroupDetail;
            }));
    }

    getUserPermissionGroupRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const userPermissionGroupDetails = res as any || {};
                return userPermissionGroupDetails;
            }));
    }

    updateUserPermissionGroup(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error(`UserPermissionGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}