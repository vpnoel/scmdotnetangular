import {BrowserModule} from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { SharedModule } from "./shared/shared.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/** Shared Component **/

/** Other Components **/
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbButtonsModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap';
import {MatPaginatorModule} from '@angular/material/paginator';

//SCM Services
import {BusService} from '../app/scm/services/bus.services';
import {CampusService} from '../app/scm/services/campus.services';
import {DashboardService} from '../app/scm/services/dashboard.services';
import {DriverService} from '../app/scm/services/driver.services';
import {GuardianService} from '../app/scm/services/guardian.services';
import {IncidentReportService} from '../app/scm/services/incidentreport.services';
import {ISDService} from '../app/scm/services/isd.services';
import {ISDStaffService} from '../app/scm/services/isdstaff.services';
import {ParentService} from '../app/scm/services/parent.services';
import {ReferralActionService} from '../app/scm/services/referralaction.services';
import {ReferralGroupService} from '../app/scm/services/referralgroup.services';
import {ReferralReasonService} from '../app/scm/services/referralreason.services';
import {ReferralReasonReportService} from '../app/scm/services/referralreasonreport.services';
import {ReferralTransactionService} from '../app/scm/services/referraltransaction.services';
import {ReferralTypeService} from '../app/scm/services/referraltype.services';
import {ReportService} from '../app/scm/services/report.services';
import {RoutesService} from '../app/scm/services/routes.services';
import {EmailTransactionService} from '../app/scm/services/sendemail.services';
import {StudentService} from '../app/scm/services/student.services';
import {TeacherService} from '../app/scm/services/teacher.services';
import {UsersService} from '../app/scm/services/users.services';
import {ReferralActionGroupService} from '../app/scm/services/referralactiongroup.services';
import {ReferralReasonGroupService} from '../app/scm/services/referralreasongroup.services';
import { SettingsService } from './../app/scm/services/settings.service';
import { UserPermissionService } from './../app/scm/services/user.permission.services';
import { RoleFeatureService} from './../app/scm/services/rolefeature.services';
import { UserPermissionGroupService} from './../app/scm/services/userpermissiongroup.services';
import { UserManagementToGroupService} from './../app/scm/services/user.management.togroup.services';
import {NgxPageScrollModule } from 'ngx-page-scroll';


// Shared Modules
import { MaterialModule } from './material.module';
import { DataService } from './shared/services/data.service';
import { HttpClientExt } from './shared/services/httpclient.services';
import { ModalService } from './shared/services/modal.services';
import { AuthGuardService } from './shared/services/auth.guard.service';
import { ReferralNotesService } from './scm/services/referralnotes.services';
import { rootRouterConfig } from './app.routes';
import { AppRoutingModule } from './app-routing.module';

//SCM Components
import { LoginFormComponent } from './scm/components/login/login.component';
import { MainBoardComponent } from './scm/components/mainboard/main.board.component';
import { CampusComponent } from './scm/components/campus/campus.component';
import { CampusFormComponent } from './scm/components/campus/campus-form/campus.form.component';
import { ConductStaffComponent } from './scm/components/conductstaff/conduct.staff.component';
import { ConductStaffFormComponent} from './scm/components/conductstaff/conductstaff-form/conductstaff.form.component';
import { DashboardComponent} from './scm/components/dashboard/dashboard.component';
import { DriverComponent } from './scm/components/driver/driver.component';
import { DriverFormComponent} from './scm/components/driver/driverform/driver.form.component';
import { ReferralComponent } from './scm/components/referral/referral.component';
import { ReferralGroupComponent } from './scm/components/referralgroup/referral.group.component';
import { ReferralGroupFormComponent } from './scm/components/referralgroup/referralgroup-form/referralgroup.form.component';
import { ReportComponent } from './scm/components/report/report.component';
import { ReferralActionComponent } from './scm/components/setup/referralaction/referral.action.component';
import { ReferralReasonComponent} from './scm/components/setup/referralreason/referral.reason.component';
import { StudentComponent } from './scm/components/student/student.component';
import { StudentFormComponent } from './scm/components/student/studentform/student.form.component';
import { SetupComponent} from './scm/components/setup/setup.component';
import { ReferralFormComponent } from './scm/components/referral/referral-form/referral.form.component';
import { ReferralViewComponent } from './scm/components/referral/referral-view/referral.view.component';
import { ReferralEditComponent} from './scm/components/referral/referral-edit/referral.edit.component';

//Modals
import { ReferralReasonModalComponent } from './scm/components/modal/referralreasonmodal/referralreasonmodal.component';
import { ReferralActionModalComponent} from './scm/components/modal/referralactionmodal/referralactionmodal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    MainBoardComponent,
    CampusComponent,
    CampusFormComponent,
    ConductStaffComponent,
    ConductStaffFormComponent,
    DashboardComponent,
    DriverComponent,
    DriverFormComponent,
    ReferralComponent,
    ReferralFormComponent,
    ReferralViewComponent,
    ReferralEditComponent,
    ReferralGroupComponent,
    ReferralGroupFormComponent,
    ReferralActionComponent,
    ReferralReasonComponent,
    ReportComponent,
    StudentComponent,
    StudentFormComponent,
    SetupComponent,
    ReferralReasonModalComponent,
    ReferralActionModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPageScrollModule,
    NgxChartsModule,
    ModalModule,
    RouterModule,
    NgbModule,
    NgbButtonsModule,
    HttpModule,
    SharedModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forRoot(rootRouterConfig, { useHash: false, anchorScrolling: 'enabled', scrollPositionRestoration: 'enabled'})
  ],
  providers: [
    AuthGuardService,
    ReferralNotesService,
    DataService,
    HttpClientExt,
    ReferralTransactionService,
    CampusService,
    TeacherService,
    GuardianService,
    StudentService,
    ReferralActionService,
    ReferralActionGroupService,
    ReferralGroupService,
    ReferralReasonService,
    ReferralReasonReportService,
    ReferralTypeService,
    RoutesService,
    DriverService,
    DashboardService,
    ReportService,
    ISDService,
    ISDStaffService,
    BusService,
    ParentService,
    IncidentReportService,
    EmailTransactionService,
    ModalService,
    UsersService,
    SettingsService,
    ReferralActionService,
    UserPermissionService,
    ReferralReasonGroupService,
    RoleFeatureService,
    UserPermissionGroupService,
    UserManagementToGroupService,],
  bootstrap: [AppComponent],
  entryComponents: [
    ReferralReasonModalComponent,
    ReferralActionModalComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule {
}
