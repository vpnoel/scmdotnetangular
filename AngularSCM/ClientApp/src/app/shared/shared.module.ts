// Core Modules
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { CommonModule } from '@angular/common';

// Shared Services
import { RedirectComponent } from './components/redirect/redirect.component';
import { HttpClientExt } from './services/httpclient.services';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
import { LandingFixService } from './services/landing-fix.service';
import { ModalService } from './services/modal.services'
import { WINDOW_PROVIDERS } from "./services/windows.service"
import { UserComponent } from './components/user/user.component';
import { ResultsComponent } from './components/result/results.component';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
    declarations: [
        RedirectComponent,
        UserComponent,
        ResultsComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule,
        NgxPageScrollModule,
        BrowserModule
    ],
    exports: [
        UserComponent,
        ResultsComponent
    ],
    providers: [
        HttpClientExt,
        AuthService,
        DataService,
        WINDOW_PROVIDERS,
        LandingFixService,
    ]
})

export class SharedModule {
    constructor() { }
} 