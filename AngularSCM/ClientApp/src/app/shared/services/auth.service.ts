
import {throwError as observableThrowError,  BehaviorSubject, ObservableInput ,  Observable } from 'rxjs';

import {catchError, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import 'rxjs';
import { IUser } from './../interfaces/IUser';
import { IISDStaff, IUserData } from '../interfaces/IISDStaff';
import { HttpClientExt } from "./httpclient.services";
import { AppSettings } from './../app.settings';
import { HttpErrorResponse } from '@angular/common/http';
import { ISDTypeEnum } from '../../scm/enum/ISDTypeEnums';

@Injectable()
export class AuthService{
    private loggedIn = new BehaviorSubject<boolean>(false);

    private authServiceUrl = `${AppSettings.SITE_HOST}/api/ISD/auth/`;

    get isLoggedIn() {
        return this.loggedIn.asObservable();
    }

    constructor(
        private router: Router,
        private http: HttpClientExt
    ) { }

    login(user: IUser): Observable<IUserData> {
        return this.http.post(this.authServiceUrl, user).pipe(
            map(res => {
                this.loggedIn.next(true);
                const data = res as IUserData;
                this.http.setAccessToken(data.Token);
                return data;
            }),
            catchError(this.handleErrorObservable),);
    }

    hasAccess(userData: IUserData, moduleId: number): boolean {

        if(userData.ISDStaff.ISDId === ISDTypeEnum.SuperAdmin) {
            return true
        }
        
        userData.Permissions.forEach(p => {
            if(p.RoleFeaturesId === moduleId && p.IsActive === true) return true;
        })

        return false
    }

    private handleErrorObservable(error: HttpErrorResponse | any) {
        console.error("Error : " + error);
        return observableThrowError(error.message || error);
    }

    logout() {
        this.loggedIn.next(false);
        this.router.navigate(['/sign-in']);
    }
}
