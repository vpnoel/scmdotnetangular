import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (localStorage.getItem("loggedUser") === null) {
        this.router.navigate(['sign-in']);
        return false;
    }
    return true;
  }
}