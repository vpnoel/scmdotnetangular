import { IReferralGroupToUser } from "../../scm/interfaces/IReferralGroupToUser";

export function getMembers(users: IReferralGroupToUser[]): string[]{
    let userNames: string[] = [];
    users.forEach(u => {
        if(u.Driver !== null) {
            userNames.push(u.Driver.DriverName);
        } else if (u.Staff !== null){
            userNames.push(u.Staff.FirstName + " " + u.Staff.LastName);
        } else if(u.Teacher !== null) {
            userNames.push(u.Teacher.Name);
        }
    })
    return userNames;
}