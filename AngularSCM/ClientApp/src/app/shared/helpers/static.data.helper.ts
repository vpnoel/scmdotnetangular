

export function timeOfDayDataListDefaults(): any {
    return [
        {
            "name": "A.M. Route",
            "value": 0
        },
        {
            "name": "P.M. Route",
            "value": 1
        }
    ];
}

export function panicButtonListDefaults(): any {
    return [
        {
            "name": "Yes",
            "value": 0
        },
        {
            "name": "No",
            "value": 1
        }
    ];
}

export function pulledVideoListDefault(): any {
    return [
        {
            "name": "Yes",
            "value": 0
        },
        {
            "name": "No",
            "value": 1
        }
    ];
}

export function VideoButtonPressedListDefaults(): any {
    return [
        {
            "name": "Yes",
            "value": 0
        },
        {
            "name": "No",
            "value": 1
        }
    ];
}

export function ReviewVideoListDefault(): any {
    return [
        {
            "name": "Yes",
            "value": 0
        },
        {
            "name": "No",
            "value": 1
        }
    ];
}

export function incidentStatusListDefault(): any {
    return [
        {
            "name": "Show All",
            "value": 0
        },
        {
            "name": "Open",
            "value": 1
        },
        {
            "name": "Close",
            "value": 2
        }
    ];
}

export function incidentStatusList(): any {
    return [
        {
            "name": "Open",
            "value": 1
        },
        {
            "name": "Close",
            "value": 2
        }
    ];
}

export function referralStatusDataListDefault(): any {
    return [
        {
            "name": "Not Approved",
            "value": 0
        },
        {
            "name": "Completed",
            "value": 1
        },
        {
            "name": "Action Required",
            "value": 2
        }
    ]
}

export function activeListDefault(): any {
    return [
        {
            "name": "Yes",
            "value": true
        },
        {
            "name": "No",
            "value": false
        }
    ];
}
