import { IIncidentReport } from "../../scm/interfaces/IIncidentReport";
import { IReferralTransaction } from "../../scm/interfaces/IReferralTransaction";
import { IEmailTransaction } from "../../scm/interfaces/IEmailTransaction";
import { IReferralTransactionReasons } from "../../scm/interfaces/IReferralTransactionReason";
import { IStudent } from "../../scm/interfaces/IStudent";
import { IUserPermissionGroup } from "../../scm/interfaces/IUserPermissionGroup";
import { IReferralNotes } from "../../scm/interfaces/IReferralNotes";

export function incidentReportDefault(): IIncidentReport {
    return {
        Id:0,
        Campus: null,
        CampusId: 0,
        CreatedBy: 0,
        DateOfReport: undefined,
        DriverId: 0,
        DtCreated: undefined,
        DtLastUpdated: undefined,
        LastUpdatedBy: 0,
        Student: null,
        StudentId: 0,
        TimeOfDay: 0,
        BusId: 0,
        Bus: null,
        DateOfIncident: undefined,
        ParentId: 0,
        Parent: null,
        ParentPhoneNumber: null,
        PanicButtonUsed: null,
        PulledVideo: null,
        DescriptionOfStudent: null,
        Grade: null,
        IncidentStatus: 0,
        IsActive: true,
        IncidentNotes: null,
        Driver: null,
        CampusName: null,
        LastName: null,
        FirstName: null,
        BusName: null,
        ParentName: null,
        DriverName: null
    }
}

export function referralTransactionDefault(): IReferralTransaction {
    return {
        Campus: null,
        CampusId: 0,
        CreatedBy: 0,
        Date: undefined,
        Driver: null,
        DriverId: 0,
        DtCreated: undefined,
        DtLastUpdated: undefined,
        LastUpdatedBy: 0,
        ReferralGroup: null,
        ReferralGroupId: 0,
        ReferralId: 0,
        ReferralReason: null,
        ReferralReasonsId: null,
        ReferralType: null,
        ReferralTypeId: 0,
        RouteId: 0,
        Student: null,
        StudentId: 0,
        TeacherId: 0,
        AssociatedVideo: '',
        Explanation: null,
        GuardianId: 0,
        IncidentLocation: '',
        IsResolved: false,
        PerceivedMotivation: '',
        ReferralStatus: 0,
        TimeOfDay: null,
        ReferralActionId: 0,
        ReferralAction: null,
        AdditionalDesc: null,
        AdminReport: null,
        DataPrivacy: null,
        ReferralCompletedByDiscProvider: true,
        ISD: null,
        BusId: 0,
        DateOfIncident: undefined,
        ParentId: 0,
        Parent: null,
        IncidentGrade: null,
        IsIncidentReport: true,
        SuspensionStartDate: undefined,
        SuspensionEndDate: undefined,
        Restitution: null,
        SchoolFeedback: null,
        VideoButtonPressed: null,
        ReviewVideo: null,
        ReferralDateClosed: undefined,
        ReferralDateEscalated: undefined,
        IsDriver: false,
        Guardian: null,
        Teacher: null,
    }
}

export function sendEmailDefault(): IEmailTransaction {
    return {
        EmailTransactionId: 0,
        FromAddress: null,
        ToAddress: null,
        CCAddress: null,
        Subject: null,
        EmailBody: null,
        Priority: 0,
        IsProcessed: true,
        Exception: null,
        EmailTypeId: 0,
        EmailPriorityType: null,
        ReferenceId: 0,
        DtCreatedLocal: undefined,
        StudentId: 0,
        CreatedBy: 'SCM',
        DtCreated: undefined,
        LastUpdatedBy: 'SCM',
        DtLastUpdated: undefined
    }
}

export function referralTransactionReasonDefaults(ownerId: number, referralTransactionId: number): IReferralTransactionReasons {
    return {
        Id: 0,
        Created: null,
        CreatedBy: ownerId,
        LastUpdated: null,
        LastUpdatedBy: null,
        Priority: null,
        ReferralReasonId: 0,
        ReferralTransactionId: referralTransactionId
    }
}

export function defaultStudent(): IStudent {
    return {
        Address: '',
        Apt: '',
        CampusId: 0,
        CampusStudentId: 0,
        City: '',
        CreatedBy: '',
        DateOfBirth: null,
        DismissalType: '',
        DistanceToSchool: '',
        DtCreated: null,
        DtLastUpdated: null,
        Ethnicity: '',
        FirstName: '',
        ForceSynchronization: 0,
        Gender: '',
        Grade: '',
        ISDId: 0,
        IsActiveRider: false,
        IsMedicationRequired: 0,
        IsRegisteredRider: false,
        LastName: '',
        LastUpdatedBy: '',
        ListOfMedication: '',
        MedicalCondition: '',
        Notations: '',
        ProviderRecordId: 0,
        SMARTAlert: false,
        SMARTAlertInbound: false,
        SmartTagId: 0,
        State: '',
        StudentExternalId: '',
        StudentId: 0,
        StudentProfileDriverDisplay: '',
        StudentStatusId: 0,
        StudentTypeId: 0,
        Zip: '',
        dtLastDismissalTypeUpdate: null
    }
}

export function userManagementDefaults(): IUserPermissionGroup {
    return {
        Id: 0,
        GroupName: '',
        IsActive: true,
        CreatedBy: 0,
        DtCreated: undefined,
        UpdatedBy: 0,
        DtLastUpdated: undefined,
        ISDId:0,
        ISD: null
    }
}

export function referralNotesDefaults(): IReferralNotes {
    return {
        Id: 0,
        ReviewerId: 0,
        CreatedBy: 0,
        DtLastUpdated: undefined,
        LastUpdatedBy: 0,
        DtCreated: undefined,
        ReferralGroupId: 0,
        Notes: '',
        ReferralTransactionId: 0,
        ReferralTransaction: null,
        ReferralGroup: null,
    }
}