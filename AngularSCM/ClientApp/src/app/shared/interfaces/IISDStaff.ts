import { IUserPermission } from "../../scm/interfaces/IUserPermission";
import { IDriver } from "../../scm/interfaces/IDriver";
import { ITeacher } from "../../scm/interfaces/ITeacher";

export interface IISDStaff {
    //isdStaffId: number,
    ISDStaffId: number,
    LoginName: string,
    FirstName: string,
    LastName: string,
    EmailAddress: string,
    Active: number,
    ISDId: number,
    IsAdmin: boolean,
    CreatedBy: string,
    DtCreated?: Date,
    lastUpdatedBy: string,
    DtLastUpdated?: Date,
    CampusId: number
}

export interface IUserData {
    ISDStaff: IISDStaff,
    Driver: IDriver,
    Teacher: ITeacher,
    Permissions: IUserPermission[],
    Token: string
}