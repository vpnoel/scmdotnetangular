
import { Component, Inject, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: "results-list",
    templateUrl: './results.component.html',
    styleUrls: [
        './results.component.scss'
    ]
})

export class ResultsComponent implements OnInit{

    @Input() objects;
    @Output() onOptionSelected: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        private authService: AuthService
    ) { }

    ngOnInit(): void {
    }

    select(o: any): void {
        this.onOptionSelected.emit(o);
    }
}