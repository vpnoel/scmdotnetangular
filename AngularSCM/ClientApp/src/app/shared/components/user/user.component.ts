
import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: "user-info",
    templateUrl: './user.component.html',
    styleUrls: [
        './user.component.scss'
    ]
})

export class UserComponent implements OnInit{

    user: string = "";

    constructor(
        private authService: AuthService
    ) { }

    ngOnInit(): void {
        let loggedUserData = JSON.parse(localStorage.loggedUser);
        this.user = loggedUserData.FirstName;
    }

    onLogout() {
        localStorage.clear()
        this.authService.logout();
    }
}