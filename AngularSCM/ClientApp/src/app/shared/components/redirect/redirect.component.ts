import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    template: ''
})

export class RedirectComponent implements OnInit {
    constructor(
        private _router: Router
    ) { }

    ngOnInit(): void {
        if (localStorage.getItem("loggedUser") !== null) {
            let data = JSON.parse(localStorage.loggedUser);
            if (data) {
                this._router.navigateByUrl('/main/dashboard');
            }
            else {
                this._router.navigateByUrl('/login');
            }
        }
        
        else {
            this._router.navigateByUrl('/login');
        }
    }
}