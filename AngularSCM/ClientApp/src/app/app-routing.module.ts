import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

//SCM COMPONENTS
import { LoginFormComponent} from './scm/components/login/login.component';
import { MainBoardComponent } from './scm/components/mainboard/main.board.component';
import { CampusComponent } from './scm/components/campus/campus.component';
import { CampusFormComponent} from './scm/components/campus/campus-form/campus.form.component';
import { ConductStaffComponent} from './scm/components/conductstaff/conduct.staff.component';
import { ConductStaffFormComponent} from './scm/components/conductstaff/conductstaff-form/conductstaff.form.component';
import { DashboardComponent } from './scm/components/dashboard/dashboard.component';
import { DriverComponent} from './scm/components/driver/driver.component';
import { DriverFormComponent} from './scm/components/driver/driverform/driver.form.component';
import { ReferralComponent} from './scm/components/referral/referral.component';
import { ReferralGroupComponent} from './scm/components/referralgroup/referral.group.component';
import { ReferralGroupFormComponent} from './scm/components/referralgroup/referralgroup-form/referralgroup.form.component';
import { ReportComponent} from './scm/components/report/report.component';
import { ReferralActionComponent} from './scm/components/setup/referralaction/referral.action.component';
import { StudentComponent} from './scm/components/student/student.component';
import { StudentFormComponent} from './scm/components/student/studentform/student.form.component';
import { SetupComponent} from './scm/components/setup/setup.component';

import { AuthGuardService as AuthGuard } from './shared/services/auth.guard.service' ;
import { ReferralFormComponent } from './scm/components/referral/referral-form/referral.form.component';
import { ReferralViewComponent } from './scm/components/referral/referral-view/referral.view.component';
import { ReferralEditComponent} from './scm/components/referral/referral-edit/referral.edit.component';

// Routes model for application.
const APP_ROUTES: Routes = [
  {
    path: 'main', component: MainBoardComponent, children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'campus', component: CampusComponent},
      {path: 'campus/view/:id', component: CampusFormComponent},
      {path: 'conductstaff', component: ConductStaffComponent},
      {path: 'driver', component: DriverComponent},
      {path: 'referral', component: ReferralComponent},
      {path: 'referral/create', component: ReferralFormComponent},
      {path: 'referral/view/:id', component: ReferralViewComponent},
      {path: 'referral/edit/:id', component: ReferralEditComponent},
      {path: 'referralgroup', component: ReferralGroupComponent},
      {path: 'referralgroup/view/:id', component: ReferralGroupFormComponent},
      {path: 'report', component: ReportComponent},
      {path: 'referralaction', component: ReferralActionComponent},
      {path: 'student', component: StudentComponent},
      {path: 'student/view/:id', component: StudentFormComponent},
      {path: 'setup', component: SetupComponent},
      {path: 'driver/view/:id', component: DriverFormComponent},
      {path: 'conductstaff/view/:id', component: ConductStaffFormComponent},
    ]
  },
  {path: 'login', component: LoginFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
