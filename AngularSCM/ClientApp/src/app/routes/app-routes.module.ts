import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

//SCM COMPONENTS
import { LoginFormComponent} from './../scm/components/login/login.component';
import { MainBoardComponent } from './../scm/components/mainboard/main.board.component';
import { CampusComponent } from './../scm/components/campus/campus.component';
import { ConductStaffComponent} from './../scm/components/conductstaff/conduct.staff.component';
import { DashboardComponent } from './../scm/components/dashboard/dashboard.component';
import { DriverComponent} from './../scm/components/driver/driver.component';
import { ReferralComponent} from './../scm/components/referral/referral.component';
import { ReferralGroupComponent} from './../scm/components/referralgroup/referral.group.component';
import { ReportComponent} from './../scm/components/report/report.component';
import { ReferralActionComponent} from './../scm/components/setup/referralaction/referral.action.component';
import { StudentComponent} from './../scm/components/student/student.component';
import { SetupComponent} from './../scm/components/setup/setup.component';

import { AuthGuardService as AuthGuard } from './../shared/services/auth.guard.service' ;

// Routes model for application. Some of the pages are loaded lazily to increase startup time.
const APP_ROUTES: Routes = [
  {
    path: 'main', component: MainBoardComponent, children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'campus', component: CampusComponent},
      {path: 'conductstaff', component: ConductStaffComponent},
      {path: 'driver', component: DriverComponent},
      {path: 'referral', component: ReferralComponent},
      {path: 'referralgroup', component: ReferralGroupComponent},
      {path: 'report', component: ReportComponent},
      {path: 'student', component: StudentComponent},
      {path: 'setup', component: SetupComponent},
    //   {path: 'referraltransaction/:id/:action', component: ReferralTransactionFormComponent},
    //   {path: 'referraltransaction/q/:startDate/:endDate', component: ReferralTransactionComponent},
    ]
  },
  {path: 'login', component: LoginFormComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES, {preloadingStrategy: PreloadAllModules}),
  ]
})
export class AppRoutesModule {
}
