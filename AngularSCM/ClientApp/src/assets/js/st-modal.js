/**
 * Created by Zeppelin on 5/6/2019.
 */
let modalTreeNodePosition = 0;

$(document).ready(function () {
    $('body').append('<div id="modal-backdrop" class="modal-backdrop-transparent modal-transition"></div>');
});

function showModalAnim() {
    $("[data-modal-tree='" + modalTreeNodePosition + "']").find('.modal-dialog').attr('class', 'modal-dialog slideInDown animated');
}

function hideModalAnim() {
    $("[data-modal-tree='" + modalTreeNodePosition + "']").find('.modal-dialog').attr('class', 'modal-dialog slideOutUp animated');
}

function revealHiddenModal() {
    $("[data-modal-tree='" + modalTreeNodePosition + "']").css('z-index', '1050');
    $("[data-modal-tree='" + modalTreeNodePosition + "']").find('.modal-dialog').attr('class', 'modal-dialog zoomIn animated');
}

function tempHideModal() {
    if (modalTreeNodePosition > 1) {
        $("[data-modal-tree='" + (modalTreeNodePosition - 1) + "']").find('.modal-dialog').attr('class', 'modal-dialog zoomOut animated');

        //wait for animation to complete then move modal to back
        setTimeout(function () {
            $("[data-modal-tree='" + (modalTreeNodePosition - 1) + "']").css('z-index', '1');
        }, 250);
    }
}

$('.modal').on('show.bs.modal', function (e) {
    modalTreeNodePosition++;
    $('#modal-backdrop').addClass('modal-backdrop-primary');
    if (modalTreeNodePosition == 1) {
        $('#modal-backdrop').addClass('modal-backdrop-primary');
    }

    $(e.currentTarget).attr('data-modal-tree', modalTreeNodePosition);
    tempHideModal();
    showModalAnim();
});

$('.modal').on('hide.bs.modal', function () {
    hideModalAnim('slideOutUp');
    modalTreeNodePosition--;
    revealHiddenModal();
    if (modalTreeNodePosition == 0) $('#modal-backdrop').removeClass('modal-backdrop-primary');
});

$('.modal').on("hidden.bs.modal", function (e) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});